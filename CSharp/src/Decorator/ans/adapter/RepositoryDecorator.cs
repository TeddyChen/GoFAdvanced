﻿/*
 * Copyright Teddysoft Technology. 
 * 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using Tw.Teddysoft.Gof.Decorator.Ans.Entity;
using Tw.Teddysoft.Gof.Decorator.Ans.Usecase;

namespace Tw.Teddysoft.Gof.Decorator.Ans.Adapter
{
    public abstract class RepositoryDecorator<T> : EsRepository<T> where T : AggregateRoot
    {
        protected EsRepository<T> component;

        public RepositoryDecorator(EsRepository<T> component)
        {
            this.component = component;
        }

        public virtual void Delete(T entity)
        {
            component.Delete(entity);
        }

        public virtual DomainEvent? GetLastEventFromStream(string streamName)
        {
            return component.GetLastEventFromStream(streamName);
        }

        public virtual List<DomainEvent> GetEventsFromStream(string streamName, long version)
        {
            return component.GetEventsFromStream(streamName, version);
        }

        public virtual T? FindById(string id)
        {
            return component.FindById(id);
        }

        public virtual void Save(T entity)
        {
            component.Save(entity);
        }

        public virtual void SaveEvent(string streamName, DomainEvent @event)
        {
            component.SaveEvent(streamName, @event);
        }
    }
}