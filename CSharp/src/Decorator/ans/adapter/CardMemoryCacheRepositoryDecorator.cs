﻿/*
 * Copyright Teddysoft Technology. 
 * 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using Tw.Teddysoft.Gof.Decorator.Ans.Entity;
using Tw.Teddysoft.Gof.Decorator.Ans.Usecase;

namespace Tw.Teddysoft.Gof.Decorator.Ans.Adapter
{
    public class CardMemoryCacheRepositoryDecorator : RepositoryDecorator<Card>
    {
        private Dictionary<string, Card> cache;

        public CardMemoryCacheRepositoryDecorator(EsRepository<Card> component) : base(component)
        {
            cache = new Dictionary<string, Card>();
        }

        public override void Save(Card card)
        {
            component.Save(card);
            cache[card.Id] = card;
        }

        public override Card? FindById(string cardId)
        {
            if (cache.ContainsKey(cardId))
            {
                Console.WriteLine("Cache hit!");
                return cache[cardId];
            }

            Card? card = component.FindById(cardId);
            if (null != card) cache[cardId] = card;
            return card;
        }

        public override void Delete(Card card)
        {
            component.Delete(card);
            cache.Remove(card.Id);
        }
    }
}