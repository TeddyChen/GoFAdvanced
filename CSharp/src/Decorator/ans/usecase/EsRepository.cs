﻿/*
 * Copyright Teddysoft Technology. 
 * 
 */
using System;
using System.Collections.Generic;
using Tw.Teddysoft.Gof.Decorator.Ans.Entity;

namespace Tw.Teddysoft.Gof.Decorator.Ans.Usecase
{
    public interface EsRepository<T> where T : AggregateRoot
    {
        T? FindById(string id);
        void Save(T entity);
        void Delete(T entity);
        DomainEvent? GetLastEventFromStream(string streamName);
        void SaveEvent(string streamName, DomainEvent @event);
        List<DomainEvent> GetEventsFromStream(string streamName, long version);
    }
}
