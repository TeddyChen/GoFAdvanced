﻿/*
 * Copyright Teddysoft Technology. 
 * 
 */
using System;

namespace Tw.Teddysoft.Gof.Decorator.Ans.Entity
{
    public interface AggregateSnapshot<T>
    {
        T GetSnapshot();
        void SetSnapshot(T snapshot);
    }

}
