﻿/*
 * Copyright Teddysoft Technology. 
 * 
 */
using System;
using System.Collections.Generic;

namespace Tw.Teddysoft.Gof.Decorator.Ans.Entity
{
    public abstract class AggregateRoot
    {
        protected string id;
        protected List<DomainEvent> domainEvents;
        protected long version;

        protected AggregateRoot() {
            this.domainEvents = new List<DomainEvent>();
            this.version = -1;
        }

        public AggregateRoot(string id)
        {
            this.id = id;
            this.domainEvents = new List<DomainEvent>();
            this.version = -1;
        }

        public AggregateRoot(IEnumerable<DomainEvent> events)
        {
            this.domainEvents = new List<DomainEvent>();
            this.version = 0;
            foreach (var e in events)
            {
                Apply(e);
            }
            ClearDomainEvents();
        }

        public long Version { get; set; }
        public string Id { get { return id; } }
        public void AddDomainEvent(DomainEvent e) { domainEvents.Add(e); }
        public List<DomainEvent> GetDomainEvents() { return domainEvents; }
        public void ClearDomainEvents() { domainEvents.Clear(); }


        public void Apply(DomainEvent @e)
        {
            When(e);
            AddDomainEvent(e);
        }

        protected abstract void When(DomainEvent @e);
    }

}
