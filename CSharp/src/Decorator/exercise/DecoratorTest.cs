﻿/*
 * Copyright 2017 TeddySoft Technology. 
 *
 */
using System;
using NUnit.Framework;
using Tw.Teddysoft.Gof.Decorator.Exercise.Adapter;
using Tw.Teddysoft.Gof.Decorator.Exercise.Entity;
using Tw.Teddysoft.Gof.Decorator.Exercise.Usecase;

namespace Tw.Teddysoft.Gof.Decorator.Exercise
{
    [TestFixture]
    public class DecoratorTest
    {
        [Test]
        public void CardEsRepository()
        {
            EsRepository<Card> cardRepository = new CardEsRepository();
            Card card = new Card("lane-001", Guid.NewGuid().ToString(), "my card");
            card.Assign("Teddy");
            card.Assign("Eiffel");
            card.Assign("Ada");
            card.Assign("Pascal");
            card.ChangeDeadline(DateTime.Now);
            cardRepository.Save(card);
            card.ChangeDescription("new card description");
            card.Unassign("Teddy");
            cardRepository.Save(card);

            Card storedCard = cardRepository.FindById(card.Id);
            Assert.AreEqual(card.Description, storedCard.Description);
            Assert.AreEqual(card.Assignees.Count, storedCard.Assignees.Count);

            // TODO
            //var snapshotted = cardRepository.GetLastEventFromStream(
            //    CardSnapshotRepositoryDecorator.GetSnapshottedStreamName(card.Id));
            //Assert.IsNull(snapshotted);
        }

        [Test]
        public void CardEsRepositoryAndSnapshotDecorator()
        {

            // TODO
            //EsRepository<Card> cardRepository = new CardSnapshotRepositoryDecorator(new CardEsRepository());
            //Card card = new Card("lane-001", Guid.NewGuid().ToString(), "my card");
            //card.Assign("Teddy");
            //card.Assign("Eiffel");
            //card.Assign("Ada");
            //card.Assign("Pascal");
            //card.ChangeDeadline(DateTime.Now);
            //cardRepository.Save(card);
            //card.ChangeDescription("new card description");
            //card.Unassign("Teddy");
            //cardRepository.Save(card);

            //Card storedCard = cardRepository.FindById(card.Id);
            //Assert.AreEqual(card.Description, storedCard.Description);
            //Assert.AreEqual(card.Assignees.Count, storedCard.Assignees.Count);

            //Snapshotted snapshotted = (Snapshotted)cardRepository.GetLastEventFromStream(
            //    CardSnapshotRepositoryDecorator.GetSnapshottedStreamName(card.Id));
            //Console.WriteLine(snapshotted.Snapshot);
        }

        [Test]
        public void CardEsRepositoryAndMemoryCacheDecorator()
        {
            // TODO
            //EsRepository<Card> cardRepository = new CardMemoryCacheRepositoryDecorator(new CardEsRepository());
            //Card card = new Card("lane-001", Guid.NewGuid().ToString(), "my card");
            //card.Assign("Teddy");
            //card.Assign("Eiffel");
            //card.Assign("Ada");
            //card.Assign("Pascal");
            //card.ChangeDeadline(DateTime.Now);
            //cardRepository.Save(card);
            //card.ChangeDescription("new card description");
            //card.Unassign("Teddy");
            //cardRepository.Save(card);

            //var storedCard = cardRepository.FindById(card.Id);
            //Assert.AreEqual(card.Description, storedCard.Description);
            //Assert.AreEqual(card.Assignees.Count, storedCard.Assignees.Count);

            //var snapshotted = cardRepository.GetLastEventFromStream(
            //    CardSnapshotRepositoryDecorator.GetSnapshottedStreamName(card.Id));
            //Assert.IsNull(snapshotted);
        }

        [Test]
        public void CardEsRepositoryAndMemoryCacheAndSnapshotDecorator()
        {
            // TODO
            //EsRepository<Card> cardRepository = new CardSnapshotRepositoryDecorator(
            //    new CardMemoryCacheRepositoryDecorator(new CardEsRepository()));
            //Card card = new Card("lane-001", Guid.NewGuid().ToString(), "my card");
            //card.Assign("Teddy");
            //card.Assign("Eiffel");
            //card.Assign("Ada");
            //card.Assign("Pascal");
            //card.ChangeDeadline(DateTime.Now);
            //cardRepository.Save(card);
            //card.ChangeDescription("new card description");
            //card.Unassign("Teddy");
            //cardRepository.Save(card);

            //var storedCard = cardRepository.FindById(card.Id);
            //Assert.AreEqual(card.Description, storedCard.Description);
            //Assert.AreEqual(card.Assignees.Count, storedCard.Assignees.Count);

            //var snapshotted = cardRepository.GetLastEventFromStream(
            //    CardSnapshotRepositoryDecorator.GetSnapshottedStreamName(card.Id));
            //Assert.IsNotNull(snapshotted);
            //var snapshot = ((Snapshotted)snapshotted).Snapshot;
            //Console.WriteLine(snapshot);
        }

        [Test]
        public void CardEsRepositoryAndSnapshotAndMemoryCacheAndDecorator()
        {
            // TODO
            //var cardRepository = new CardSnapshotRepositoryDecorator(
            //    new CardMemoryCacheRepositoryDecorator(new CardEsRepository()));
            //var card = new Card("lane-001", Guid.NewGuid().ToString(), "my card");
            //card.Assign("Teddy");
            //card.Assign("Eiffel");
            //card.Assign("Ada");
            //card.Assign("Pascal");
            //card.ChangeDeadline(DateTime.Now);
            //cardRepository.Save(card);
            //card.ChangeDescription("new card description");
            //card.Unassign("Teddy");
            //cardRepository.Save(card);

            //var storedCard = cardRepository.FindById(card.Id);
            //Assert.AreEqual(card.Description, storedCard.Description);
            //Assert.AreEqual(card.Assignees.Count, storedCard.Assignees.Count);

            //var snapshotted = cardRepository.GetLastEventFromStream(
            //    CardSnapshotRepositoryDecorator.GetSnapshottedStreamName(card.Id));
            //Assert.IsNotNull(snapshotted);
            //var snapshot = ((Snapshotted)snapshotted).Snapshot;
            //Console.WriteLine(snapshot);
        }
    }
}
