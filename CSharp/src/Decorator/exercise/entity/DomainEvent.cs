﻿/*
 * Copyright Teddysoft Technology. 
 * 
 */
using System;

namespace Tw.Teddysoft.Gof.Decorator.Exercise.Entity
{
    public record DomainEvent
    {
        Guid Id { get; }
        DateTime OccurredOn { get; }
        string AggregateId { get; }
    }

    public record Snapshotted(
        string AggregateId,
        string Snapshot,
        long Version,
        Guid Id,
        DateTime OccurredOn
    ) : DomainEvent;
}
