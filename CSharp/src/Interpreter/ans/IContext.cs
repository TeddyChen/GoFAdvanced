﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;

namespace Tw.Teddysoft.Gof.Interpreter.Ans
{
    public interface IContext
    {
        void skipToken(String token);
        String nextToken();
        String currentToken();

        bool containsBlock(String blockID);
        void putBlock(String blockID, INode blockNode);
        INode getBlock(String blockID);
    }
}
