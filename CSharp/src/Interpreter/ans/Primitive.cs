﻿/*
* Copyright 2017 TeddySoft Technology. 
* 
*/
using System;

namespace Tw.Teddysoft.Gof.Interpreter.Ans
{
    /*
     * <primitive> ::= DEL <file name> | CD <folder name> 
     * 					| CALL <block ID>
     */
    public class Primitive : INode
    {
        private String _cmd;
        private String _text;
        private IContext _context;

        public void parse(IContext context)
        {
            _context = context;
            _cmd = context.currentToken();
            context.skipToken(_cmd);

            switch (_cmd)
            {
                case "DEL":
                case "CD":
                case "CALL":
                    _text = context.currentToken();
                    context.nextToken();
                    break;
                default:
                    Console.WriteLine("Undefined primitive : " + _cmd);
                    break;
            }
        }

        public void execute()
        {
            switch (_cmd)
            {
                case "DEL":
                    Console.WriteLine("Delete file: '"
                            + _text + "'");
                    break;
                case "CD":
                    Console.WriteLine("Change to directory: '"
                            + _text + "'");
                    break;
                case "CALL":
                    Console.WriteLine("Call block, ID = '"
                            + _text + "'");
                    INode block = _context.getBlock(_text);
                    if (null == block)
                        Console.WriteLine("Invalid block ID = '"
                                + _text + "'");
				    else
					    block.execute();
                    break;
                default:
                    throw new ApplicationException("Infeasible  path.");
            }
        }
    }
}
