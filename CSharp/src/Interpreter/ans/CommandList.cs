﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;
using System.Collections.Generic;

namespace Tw.Teddysoft.Gof.Interpreter.Ans
{
    /*
     * <command list> ::= <command>* END
     */
    public class CommandList : INode
    {
        private IList<INode> _commands = new List<INode>();

        public void parse(IContext context)
        {
            while (true)
            {
                if (null == context.currentToken())
                {
                    Console.WriteLine("Missing 'END'");
                    break;
                }
                else if (context.currentToken().Equals("END"))
                {
                    context.skipToken("END");
                    break;
                }
                else
                {
                    INode command = new Command();
                    command.parse(context);
                    _commands.Add(command);
                }
            }
        }

        public void execute()
        {
            foreach (INode cmd in _commands)
            {
                cmd.execute();
            }
        }
    }
}
