﻿/*
* Copyright 2017 TeddySoft Technology. 
* 
*/
using System;
using NUnit.Framework;

namespace Tw.Teddysoft.Gof.Interpreter.Ans
{
    [TestFixture]
    public class ScriptTest
    {
        //e.g., C:\Users\teddy\Workspace\GoFAdvanced\CSharp\bin\Debug\
        private String currentDir = System.AppDomain.CurrentDomain.BaseDirectory;

        [Test]
        public void testSimpleScript() { 
            INode script = new Script();
            script.parse(new Context(currentDir + @"..\..\etc\simple.txt"));
		    script.execute();
	    }

        [Test]
        public void testBlockScript() 
        {
            INode script = new Script();
            script.parse(new Context(currentDir + @"..\..\etc\block.txt"));
		    script.execute();
	    }

        [Test]
        public void testNestedBlockScript() 
        {
            INode script = new Script();
            script.parse(new Context(currentDir + @"..\..\etc\nestedblock.txt"));
		    script.execute();
	    }

	    [Test]
        public void testInvalidBlockID() 
        {
            INode script = new Script();
            script.parse(new Context(currentDir + @"..\..\etc\invalidBlockID.txt"));
		    script.execute();
	    }
    }
}
