﻿/*
* Copyright 2017 TeddySoft Technology. 
* 
*/
using System;

namespace Tw.Teddysoft.Gof.Interpreter.Exercise
{
    public interface INode
    {
        void parse(IContext context);
        void execute();
    }
}
