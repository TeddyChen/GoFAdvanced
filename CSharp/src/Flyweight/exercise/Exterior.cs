﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;
using System.Threading;

namespace Tw.Teddysoft.Gof.Flyweight.Exercise
{
    public class Exterior
    {
        private String _source = null;
        private ThreeDModel _model = null;

        private Color _color = null;
        private Style _style = null;

        public void draw()
        {
            /* 
              use the 3D model from the _source, 
              Color, and Style to show the 3D model on the screen.		
            */
        }

        public Exterior clone()
        {
            Exterior newObject = null;
            memoryCopy(this, newObject);
            return newObject;
        }

        public Exterior(String aFileName)
        {
            _source = aFileName;

            // loading exterior from the file needs 20 ms
            Thread.Sleep(20);

        }

        public String getSource()
        {
            return _source;
        }

        private void memoryCopy(Exterior aSrource, Exterior aDest)
        {
            // copy exterior from memory needs 5 ms
            Thread.Sleep(5);
        }
    }
}
