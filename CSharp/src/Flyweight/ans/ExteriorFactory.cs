﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Tw.Teddysoft.Gof.Flyweight.Ans
{
    public class ExteriorFactory
    {
        private IDictionary<String, Exterior> _pool = new Dictionary<String, Exterior>();
        private static ExteriorFactory _instance = new ExteriorFactory();

        public static ExteriorFactory getInstance()
        {
            return _instance;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Exterior getFlyweight(String aFileName)
        {
            if (null == aFileName)
                throw new ApplicationException("File name cannot be null.");

            if (!_pool.ContainsKey(aFileName))
            {
                _pool.Add(aFileName, new SharedExterior(aFileName));
            }

            return _pool[aFileName];
        }

        public int getFlyweightSize()
        {
            return _pool.Count;
        }
    }
}
