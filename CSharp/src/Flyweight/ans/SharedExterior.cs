﻿﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;
using System.Threading;

namespace Tw.Teddysoft.Gof.Flyweight.Ans
{
    public class SharedExterior : Exterior
    {
        public String _source = null;
        public ThreeDModel _model = null;

        public void draw(Color aColor, Style aStyle)
        {
            /* 
		      use the 3D model from the _source, 
		      the Color, and the Style arguments to show 
		      the 3D model on the screen.		
		    */
        }

        public SharedExterior(String aFileName)
        {
            _source = aFileName;
            // loading exterior from the file needs 20 ms
            Thread.Sleep(20);
        }

        public String getSource()
        {
            return _source;
        }
    }
}
