﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;

namespace Tw.Teddysoft.Gof.Flyweight.Ans
{
    public interface Exterior
    {
        void draw(Color aColor, Style aStyle);
    }
}
