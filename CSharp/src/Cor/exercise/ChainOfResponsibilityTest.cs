﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;
using NUnit.Framework;
using System.Text;

namespace Tw.Teddysoft.Gof.Cor.Exercise
{
    [TestFixture]
    public class ChainOfResponsibilityTest
    {
        private Service mySqlCritical;
        private Service userDefinedCritical;
        private Service userDefinedOk;
        private StringBuilder result;

        [SetUp]
        public void setup() 
        {
            mySqlCritical = new Service(ServiceType.Database, "MySQL 6.5");
            userDefinedCritical = new Service(ServiceType.UserDefined, "My Microservice");
            userDefinedOk = new Service(ServiceType.UserDefined, "My Microservice");
            result = new StringBuilder();
		
		    for(int i = 0; i< 10; i++){
			    mySqlCritical.incRetry();
			    userDefinedCritical.incRetry();
		    }

            DateTime date = new DateTime(2017, 7, 19);
            mySqlCritical.setCurrentState(State.Critical, date);
		    userDefinedCritical.setCurrentState(State.Critical, date);
		    userDefinedOk.setCurrentState(State.Ok, date);
	    }

        [Test]
        public void when_too_many_attepmts_and_critical_state_over_one_day_for_database_service()
        {
            //TODO
            //String expectedResult = "Too many attempts please try again later.\n" +
            //    "Escalation due to critical state over one day.\n";

            //ServiceHandler retryCountHandler = new RetryCountHandler();
            //ServiceHandler criticalDurationHandler = new CriticalDurationHandler();
            //retryCountHandler.setNext(criticalDurationHandler);

            //retryCountHandler.handle(mySqlCritical, result);
            //Assert.AreEqual(expectedResult, result.ToString());
        }

        [Test]
        public void when_too_many_attepmts_and_critical_state_over_one_day_for_userdefined_serivce()
        {
            String expectedResult = "Too many attempts please try again later.\n" +
                    "Escalation due to critical state over one day.\n" +
                    "State change from Pending to Critical.\n";

            //TODO
            //ServiceHandler retryCountHandler = new RetryCountHandler();
            //ServiceHandler criticalDurationHandler = new CriticalDurationHandler();
            //ServiceHandler stateChangeHandler = new StateChangeHandler();

            //retryCountHandler.setNext(criticalDurationHandler);
            //criticalDurationHandler.setNext(stateChangeHandler);

            //retryCountHandler.handle(userDefinedCritical, result);
            //Assert.AreEqual(expectedResult, result.ToString());
        }

        [Test]
        public void when_too_many_attepmts_and_Ok_state_over_one_day_for_userdefined_serivce_status()
        {
            String expectedResult = "State change from Pending to Ok.\n";

            //TODO
            //ServiceHandler retryCountHandler = new RetryCountHandler();
            //ServiceHandler criticalDurationHandler = new CriticalDurationHandler();
            //ServiceHandler stateChangeHandler = new StateChangeHandler();

            //retryCountHandler.setNext(criticalDurationHandler);
            //criticalDurationHandler.setNext(stateChangeHandler);

            //retryCountHandler.handle(userDefinedOk, result);
            //    Assert.AreEqual(expectedResult, result.ToString());
        }
    }
}
