﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;
using System.Collections.Generic;

namespace Tw.Teddysoft.Gof.Cor.Exercise
{
    public enum State
    {
        Pending,
        Ok,
        Warning,
        Critical
    }
}
