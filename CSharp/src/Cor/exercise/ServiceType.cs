﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;

namespace Tw.Teddysoft.Gof.Cor.Exercise
{
    public enum ServiceType
    {
        Database,
        Http,
        Email,
        Ftp,
        UserDefined
    }
}
