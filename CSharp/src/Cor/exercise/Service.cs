﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;

namespace Tw.Teddysoft.Gof.Cor.Exercise
{
    public class Service
    {
        private String name;
        private ServiceType type;
        private int retry = 0;
        private State currentState;
        private State previousState;
        private DateTime stateChange;
        private int checkCount = 0;

        public Service(ServiceType type, String name)
        {
            this.type = type;
            this.name = name;
            currentState = State.Pending;
            previousState = State.Pending;
        }

        public ServiceType getType()
        {
            return type;
        }

        public String getName()
        {
            return name;
        }

        public State getCurrentState()
        {
            return currentState;
        }

        public void setCurrentState(State newState, DateTime date)
        {
            stateChange = date;
            previousState = currentState;
            currentState = newState;
        }

        public State getPreviousState()
        {
            return previousState;
        }

        public void incRetry()
        {
            retry++;
        }
        public void resetRetry()
        {
            retry = 0;
        }

        public int getRetry()
        {
            return retry;
        }

        public DateTime getLastStateChange()
        {
            return stateChange;
        }

        public long getCheckedCount()
        {
            return checkCount;
        }

        public void incCheckedCount()
        {
            checkCount++;
        }

        public bool isStateChanged()
        {
            return currentState == previousState ? false : true;
        }
    }
}
