﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;
using NUnit.Framework;
using System.Text;

namespace Tw.Teddysoft.Gof.Cor.Exercise
{
    [TestFixture]
    public class MonitorTest
    {
        private Service mySqlCritical;
        private Service userDefinedCritical;
        private Service userDefinedOk;

        [SetUp]
        public void setup()
        {
            mySqlCritical = new Service(ServiceType.Database, "MySQL 6.5");
            userDefinedCritical = new Service(ServiceType.UserDefined, "My Microservice");
            userDefinedOk = new Service(ServiceType.UserDefined, "My Microservice");

            for (int i = 0; i < 10; i++)
            {
                mySqlCritical.incRetry();
                userDefinedCritical.incRetry();
            }

            DateTime date = new DateTime(2017, 7, 19);
            mySqlCritical.setCurrentState(State.Critical, date);
            userDefinedCritical.setCurrentState(State.Critical, date);
            userDefinedOk.setCurrentState(State.Ok, date);
        }

        [Test]
        public void when_too_many_attepmts_and_critical_state_over_one_day_for_database_service()
        {
            String expectedResult = "Too many attempts please try again later.\n" +
                "Escalation due to critical state over one day.\n";

            Monitor monitor = new Monitor();
            Assert.AreEqual(expectedResult, monitor.handleServiceCheck(mySqlCritical));
        }

        [Test]
        public void when_too_many_attepmts_and_critical_state_over_one_day_for_userdefined_serivce()
        {
            String expectedResult = "Too many attempts please try again later.\n" +
                                     "Escalation due to critical state over one day.\n" +
                                     "State change from Pending to Critical.\n";

            Monitor monitor = new Monitor();
            Assert.AreEqual(expectedResult, monitor.handleServiceCheck(userDefinedCritical));
        }


        [Test]
        public void when_too_many_attepmts_and_Ok_state_over_one_day_for_userdefined_serivce_status()
        {
            String expectedResult = "State change from Pending to Ok.\n";

            Monitor monitor = new Monitor();
            Assert.AreEqual(expectedResult, monitor.handleServiceCheck(userDefinedOk));
        }
    }
}