﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;
using System.Text;

namespace Tw.Teddysoft.Gof.Cor.Ans
{
    public class RetryCountHandler : ServiceHandler
    {
        protected override void internalHandler(Service service, StringBuilder result)
        {
            if (service.getRetry() >= 5)
            {
                // auto adjust the check interval
                result.Append("Too many attempts please try again later.\n");
            }
        }
    }
}
