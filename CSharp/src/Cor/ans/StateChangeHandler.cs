﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;
using System.Text;

namespace Tw.Teddysoft.Gof.Cor.Ans
{
    public class StateChangeHandler : ServiceHandler
    {

    protected override void internalHandler(Service service, StringBuilder result)
    {
        if (service.isStateChanged())
        {
            result.Append("State change from " + service.getPreviousState() +
                    " to " + service.getCurrentState() + ".\n");
        }
    }
}
}
