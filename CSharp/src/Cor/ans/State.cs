﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;
using System.Collections.Generic;

namespace Tw.Teddysoft.Gof.Cor.Ans
{
    public enum State
    {
        Pending,
        Ok,
        Warning,
        Critical
    }
}
