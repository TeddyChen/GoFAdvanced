﻿/*
 * Copyright Teddysoft Technology. 
 * 
 */
using System;
using NUnit.Framework;
using System.Collections.Generic;
using Tw.Teddysoft.Gof.Visitor.Ans.Visitor;

namespace Tw.Teddysoft.Gof.Visitor.Ans
{
    using System.IO;
    using NUnit.Framework;
    using Tw.Teddysoft.Gof.Visitor.Ans.Entity;

    [TestFixture]
    public class VisitorTest
    {
        [Test]
        public void Test_PlainTextVisitor()
        {
            Feature feature = Feature.New("Visitor design pattern");

            feature.NewScenario("Simple scenario")
                    .Given("the tax excluded price of a computer is $20,000", () => {
                    })
                    .And("the VAT rate is 5%", () => {
                    })
                    .When("I buy the computer", () => {
                    })
                    .Then("I need to pay $21,000", () => {
                    }).Execute();


            PlainTextVisitor plainTextVisitor = new PlainTextVisitor();
            feature.Accept(plainTextVisitor);
            plainTextVisitor.WriteToFile("./plaintext.txt"); // the file is in the bin/debug folder
        }
    }
}
