﻿/*
 * Copyright Teddysoft Technology. 
 * 
 */
using System;
using Tw.Teddysoft.Gof.Visitor.Ans.Visitor;

namespace Tw.Teddysoft.Gof.Visitor.Ans.Entity
{
    public class Given : Step
    {
        public Given(string description, Action callback)
            : base(description, callback)
        {
        }

        public override string GetName()
        {
            return "Given";
        }
    }
}
