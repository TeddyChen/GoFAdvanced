﻿/*
 * Copyright Teddysoft Technology. 
 * 
 */
using System;

namespace Tw.Teddysoft.Gof.Visitor.Ans.Entity
{
    public enum StepExecutionOutcome
    {
        Pending,
        Success,
        Failure,
        Skipped
    }
}
