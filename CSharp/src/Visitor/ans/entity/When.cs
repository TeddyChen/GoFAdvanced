﻿/*
 * Copyright Teddysoft Technology. 
 * 
 */
using System;
using Tw.Teddysoft.Gof.Visitor.Ans.Visitor;

namespace Tw.Teddysoft.Gof.Visitor.Ans.Entity
{
    public class When : Step
    {
        public When(string description, Action callback)
            : base(description, callback)
        {
        }

        public override string GetName()
        {
            return "When";
        }
    }
}
