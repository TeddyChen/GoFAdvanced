﻿/*
 * Copyright Teddysoft Technology. 
 * 
 */
using System;

namespace Tw.Teddysoft.Gof.Visitor.Ans.Visitor
{
    public interface SpecificationElementVisitor
    {
        void Visit(SpecificationElement element);
    }

}
