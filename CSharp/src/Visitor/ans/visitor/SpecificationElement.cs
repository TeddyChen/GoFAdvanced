﻿/*
 * Copyright Teddysoft Technology. 
 * 
 */
using System;

namespace Tw.Teddysoft.Gof.Visitor.Ans.Visitor
{
    public interface SpecificationElement
    {
        string GetName();
        void Accept(SpecificationElementVisitor visitor);
    }

}
