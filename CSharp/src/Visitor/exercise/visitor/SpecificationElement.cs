﻿/*
 * Copyright Teddysoft Technology. 
 * 
 */
using System;

namespace Tw.Teddysoft.Gof.Visitor.Exercise.Visitor
{
    public interface SpecificationElement
    {
        string GetName();
        // TODO
        //void Accept(SpecificationElementVisitor visitor);
    }

}
