﻿/*
 * Copyright Teddysoft Technology. 
 * 
 */
using System;
using System.IO;
using System.Text;
using Tw.Teddysoft.Gof.Visitor.Exercise.Entity;

namespace Tw.Teddysoft.Gof.Visitor.Exercise.Visitor
{
    public class PlainTextVisitor : SpecificationElementVisitor
    {
        private readonly StringBuilder output = new StringBuilder();

        // TODO
        //public void Visit(SpecificationElement element)
        //{
        //    switch (element)
        //    {
        //        case Feature feature:
        //            output.Append(feature.FeatureText());
        //            break;
        //        case Scenario scenario:
        //            output.Append($"\n\n{scenario.FullText()}");
        //            break;
        //        case Step step:
        //            output.Append($"\n[{step.GetResult()}] {step.GetName()} {step.Description}");
        //            break;
        //        default:
        //            break;
        //    }
        //}

        public string GetOutput()
        {
            return output.ToString();
        }

        public void WriteToFile(string fileName)
        {
            using (StreamWriter writer = new StreamWriter(fileName))
            {
                writer.Write(output.ToString());
            }
        }
    }
}
