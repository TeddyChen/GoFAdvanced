﻿/*
 * Copyright Teddysoft Technology. 
 * 
 */
using System;
using System.Collections.Generic;
using System.Text;
using Tw.Teddysoft.Gof.Visitor.Exercise.Visitor;

namespace Tw.Teddysoft.Gof.Visitor.Exercise.Entity
{
    public class Feature 
    {
        public const string KEYWORD = "Feature";
        private readonly string name;
        private readonly string description;
        private readonly List<Scenario> scenarios;

        public static Feature New(string name)
        {
            return new Feature(name);
        }

        public static Feature New(string name, string description)
        {
            return new Feature(name, description);
        }

        public Feature(string name)
            : this(name, "")
        {
        }

        public Feature(string name, string description)
        {
            this.name = name;
            this.description = description;
            scenarios = new List<Scenario>();
        }

        public List<Scenario> Scenarios => scenarios;

        public string Description => description;

        public Scenario NewScenario(string name)
        {
            var scenario = new Scenario(name);
            scenarios.Add(scenario);
            return scenario;
        }

        public string FeatureText()
        {
            var sb = new StringBuilder();
            sb.Append($"Feature: {name}");

            if (!string.IsNullOrEmpty(description))
                sb.Append($"\n\n{description}");

            return sb.ToString();
        }

        private string ScenariosText()
        {
            var sb = new StringBuilder();
            foreach (var scenario in scenarios)
            {
                sb.Append($"{scenario.FullText()}\n");
            }
            return sb.ToString();
        }

        public string FullText()
        {
            var sb = new StringBuilder(FeatureText());
            sb.Append(ScenariosText());
            return sb.ToString();
        }

        public string GetName()
        {
            return name;
        }
    }
}
