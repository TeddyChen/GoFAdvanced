﻿/*
 * Copyright Teddysoft Technology. 
 * 
 */
using System;
using Tw.Teddysoft.Gof.Visitor.Exercise.Visitor;

namespace Tw.Teddysoft.Gof.Visitor.Exercise.Entity
{
    public abstract class Step
    {
        private string description;
        private Result result;
        private Action callback;

        public Step(string description, Action callback)
        {
            this.description = description;
            this.callback = callback;
        }

        public Action Callback
        {
            get { return callback; }
        }

        public string Description
        {
            get { return description; }
        }

        public Result GetResult()
        {
            return result;
        }

        public void SetResult(Result result)
        {
            this.result = result;
        }

        public abstract string GetName();

    }
}
