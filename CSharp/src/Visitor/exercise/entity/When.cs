﻿/*
 * Copyright Teddysoft Technology. 
 * 
 */
using System;
using Tw.Teddysoft.Gof.Visitor.Exercise.Visitor;

namespace Tw.Teddysoft.Gof.Visitor.Exercise.Entity
{
    public class When : Step
    {
        public When(string description, Action callback)
            : base(description, callback)
        {
        }

        public override string GetName()
        {
            return "When";
        }
    }
}
