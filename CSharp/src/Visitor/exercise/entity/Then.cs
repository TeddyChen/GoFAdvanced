﻿/*
 * Copyright Teddysoft Technology. 
 * 
 */
using System;
using Tw.Teddysoft.Gof.Visitor.Exercise.Visitor;

namespace Tw.Teddysoft.Gof.Visitor.Exercise.Entity
{
    public class Then : Step
    {
        public Then(string description, Action callback)
            : base(description, callback)
        {
        }

        public override string GetName()
        {
            return "Then";
        }
    }
}
