﻿/*
 * Copyright Teddysoft Technology. 
 * 
 */
using System;

namespace Tw.Teddysoft.Gof.Visitor.Exercise.Entity
{
    public enum StepExecutionOutcome
    {
        Pending,
        Success,
        Failure,
        Skipped
    }
}
