﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;
using NUnit.Framework;

namespace Tw.Teddysoft.Gof.Prototype.Ans
{
    [TestFixture]
    public class MonsterTest
    {

        [Test]
        public void testCreateMonstersAfterApplyingPrototype()
        {
            int max = 50;
            DateTime start = DateTime.Now;
            Monster monster = new Monster(100, 100, "./monster.xml");

            for (int i = 0; i < max; i++)
            {
                Monster clone = monster.clone();
            }
            DateTime end = DateTime.Now;
            TimeSpan ts = end - start;

            Console.WriteLine("Create " + max + " Monster needs: " + ts.TotalMilliseconds + " ms");
        }


        [Test]
        public void testMasterBeforePrototype()
        {
            Monster monster = new Monster(100, 100, "/master.xml");

            Console.WriteLine("Original Master...");
            monster.display();

            Console.WriteLine("Master was hit.");
            monster.hit();
            Monster clone1 = new Monster(monster.getHP(), monster.getPower(), monster.getExterior().getSource());
            Monster clone2 = new Monster(monster.getHP(), monster.getPower(), monster.getExterior().getSource());

            Console.WriteLine("Clone two master, clone1 and clone 2.");

            Console.WriteLine("Original Master...");
            monster.display();
            Console.WriteLine("Clone 1...");
            clone1.display();
            Console.WriteLine("Clone 2...");
            clone2.display();

            Console.WriteLine("Master was hit twice.");
            monster.hit();
            monster.hit();

            Console.WriteLine("Clone one master, clone3.");
            Monster clone3 = new Monster(monster.getHP(), monster.getPower(), monster.getExterior().getSource());

            Console.WriteLine("Original Master...");
            monster.display();
            Console.WriteLine("Clone 1...");
            clone1.display();
            Console.WriteLine("Clone 2...");
            clone2.display();
            Console.WriteLine("Clone 3...");
            clone3.display();
        }


        [Test]
        public void testMasterClone()
        {

            Monster master = new Monster(100, 100, "/master.xml");
            Console.WriteLine("Original Master...");
            master.display();

            Console.WriteLine("Master was hit.");
            master.hit();
            Monster clone1 = master.clone();
            Monster clone2 = master.clone();

            Console.WriteLine("Clone two master, clone1 and clone 2.");

            Console.WriteLine("Original Master...");
            master.display();
            Console.WriteLine("Clone 1...");
            clone1.display();
            Console.WriteLine("Clone 2...");
            clone2.display();


            Console.WriteLine("Master was hit twice.");
            master.hit();
            master.hit();

            Console.WriteLine("Clone one master, clone3.");
            Monster clone3 = master.clone();


            Console.WriteLine("Original Master...");
            master.display();
            Console.WriteLine("Clone 1...");
            clone1.display();
            Console.WriteLine("Clone 2...");
            clone2.display();
            Console.WriteLine("Clone 3...");
            clone3.display();

        }
    }
}
