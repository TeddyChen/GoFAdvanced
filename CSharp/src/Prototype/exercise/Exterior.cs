﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;
using System.Threading;

namespace Tw.Teddysoft.Gof.Prototype.Exercise
{
    public class Exterior
    {
        public String source = null;

        public Exterior(String aFileName)
        {
            source = aFileName;

            // mock code: loading exterior from the file needs 20 ms
            Thread.Sleep(20);
        }

        public String getSource()
        {
            return source;
        }
    }
}
