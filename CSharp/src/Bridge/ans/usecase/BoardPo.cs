﻿/*
 * Copyright TeddySoft Technology. 
 * 
 */
using System;
using System.Collections.Generic;

namespace Tw.Teddysoft.Gof.Bridge.Ans.Usecase
{
    public class BoardPo : PersistentObject
    {
        private string id;
        private string boardName;
        private List<DomainEventPo> domainEventPos;

        public BoardPo(string boardId, string boardName, List<DomainEventPo> domainEventPos)
        {
            this.id = boardId;
            this.boardName = boardName;
            this.domainEventPos = domainEventPos;
        }

        public string Id
        {
            get { return id; }
            set { id = value; }
        }

        public List<DomainEventPo> DomainEvents
        {
            get { return domainEventPos; }
        }

        public string BoardName
        {
            get { return boardName; }
            set { boardName = value; }
        }
    }
}

