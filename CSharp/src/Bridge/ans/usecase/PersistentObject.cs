﻿/*
 * Copyright TeddySoft Technology. 
 * 
 */
using System;
using System.Collections.Generic;
using Tw.Teddysoft.Gof.Bridge.Ans.Entity;

namespace Tw.Teddysoft.Gof.Bridge.Ans.Usecase
{
    public interface PersistentObject
    {
        string Id { get; }
        List<DomainEventPo> DomainEvents { get; }
    }

}

