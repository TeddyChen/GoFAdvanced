﻿/*
 * Copyright TeddySoft Technology. 
 * 
 */
using System;
namespace Tw.Teddysoft.Gof.Bridge.Ans.Entity
{
    public class Card : AggregateRoot
    {
        private string cardName;
        private string assignee;

        public Card(string cardId, string name, string assignee) : base(cardId)
        {
            this.cardName = name;
            this.assignee = assignee;
        }

        public string CardName
        {
            get { return cardName; }
            set { cardName = value; }
        }

        public string Assignee
        {
            get { return assignee; }
            set { assignee = value; }
        }
    }
}

