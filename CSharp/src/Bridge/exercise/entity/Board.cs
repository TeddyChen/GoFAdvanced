﻿/*
 * Copyright TeddySoft Technology. 
 * 
 */
using System;
namespace Tw.Teddysoft.Gof.Bridge.Exercise.Entity
{
    public class Board : AggregateRoot {
        private String boardName;

        public Board(String boardId, String boardName) : base(boardId)
        {
            this.boardName = boardName;
        }

        public string BoardName
        {
            get { return boardName; }
            set { boardName = value; }
        }
    }
}

