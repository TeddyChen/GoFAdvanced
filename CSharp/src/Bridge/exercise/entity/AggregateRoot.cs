﻿/*
 * Copyright TeddySoft Technology. 
 * 
 */
using System;
using System.Collections.Generic;
using System.IO;

namespace Tw.Teddysoft.Gof.Bridge.Exercise.Entity
{
    public abstract class AggregateRoot
    {
        private String id;
        protected List<DomainEvent> domainEvents;

        public AggregateRoot(String id)
        {
            this.id = id;
            domainEvents = new List<DomainEvent>();
        }

        public string Id
        {
            get { return id; }
        }


        public void addDomainEvent(DomainEvent eventObj)
        {
            domainEvents.Add(eventObj);
        }

        public List<DomainEvent> getDomainEvents()
        {
            return domainEvents;
        }

        public void clearDomainEvents()
        {
            domainEvents.Clear();
        }
    }
}
