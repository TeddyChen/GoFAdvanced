﻿/*
 * Copyright TeddySoft Technology. 
 * 
 */
using System;
using System.Collections.Generic;

namespace Tw.Teddysoft.Gof.Bridge.Exercise.Usecase
{
    public class CardPo : PersistentObject
    {
        private string id;
        private string name;
        private string assignee;
        private List<DomainEventPo> domainEventPos;

        public CardPo(string cardId, string name, string assignee, List<DomainEventPo> domainEventPos)
        {
            this.id = cardId;
            this.name = name;
            this.assignee = assignee;
            this.domainEventPos = domainEventPos;
        }

        public string Id
        {
            get { return id; }
            set { id = value; }
        }

        public List<DomainEventPo> DomainEvents
        {
            get { return domainEventPos; }
            set { domainEventPos = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Assignee
        {
            get { return assignee; }
            set { assignee = value; }
        }
    }

}

