﻿/*
 * Copyright TeddySoft Technology. 
 * 
 */
using System;
using System.Collections.Generic;
using Tw.Teddysoft.Gof.Bridge.Exercise.Entity;

namespace Tw.Teddysoft.Gof.Bridge.Exercise.Usecase
{
    public interface RepositoryImpl<T> where T : PersistentObject
    {
        void Save(T po);
        void Delete(string id);
        T? FindById(string id);
        List<T> FindAll();
    }
}

