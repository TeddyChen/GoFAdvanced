﻿/*
 * Copyright TeddySoft Technology. 
 * 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using Tw.Teddysoft.Gof.Bridge.Exercise.Entity;
using Tw.Teddysoft.Gof.Bridge.Exercise.Usecase;

namespace Tw.Teddysoft.Gof.Bridge.Exercise.Adapter
{
    public class FakeMySQLRepositoryImpl<T> : RepositoryImpl<T> where T : PersistentObject
    {
        private List<T> stores = new List<T>();

        public void Save(T po)
        {
            stores.Add(po);
        }

        public void Delete(string id)
        {
            stores.RemoveAll(x => x.Id == id);
        }

        public T? FindById(string id)
        {
            return stores.FirstOrDefault(x => x.Id == id);
        }

        public List<T> FindAll()
        {
            return stores.AsReadOnly().ToList();
        }
    }
}

