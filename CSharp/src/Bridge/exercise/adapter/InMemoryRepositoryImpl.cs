﻿/*
 * Copyright TeddySoft Technology. 
 * 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using Tw.Teddysoft.Gof.Bridge.Exercise.Entity;
using Tw.Teddysoft.Gof.Bridge.Exercise.Usecase;

namespace Tw.Teddysoft.Gof.Bridge.Exercise.Adapter
{
    public class InMemoryRepositoryImpl<T> : RepositoryImpl<T> where T : PersistentObject
    {
        private Dictionary<string, T> entities = new Dictionary<string, T>();

        public void Save(T po)
        {
            entities[po.Id] = po;
        }

        public void Delete(string id)
        {
            entities.Remove(id);
        }

        public T? FindById(string id)
        {
            T po;
            if (entities.TryGetValue(id, out po))
            {
                return po;
            }
            return default;
        }

        public List<T> FindAll()
        {
            return entities.Values.ToList();
        }
    }
}

