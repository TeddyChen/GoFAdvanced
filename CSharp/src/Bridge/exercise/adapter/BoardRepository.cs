﻿/*
 * Copyright TeddySoft Technology. 
 * 
 */
using System;
using System.Collections.Generic;
using Tw.Teddysoft.Gof.Bridge.Exercise.Entity;
using Tw.Teddysoft.Gof.Bridge.Exercise.Usecase;

namespace Tw.Teddysoft.Gof.Bridge.Exercise.Adapter
{
    public class BoardRepository : Repository<Board, BoardPo>
    {
        public BoardRepository(RepositoryImpl<BoardPo> implementation) : base(implementation) { }

        public override void Save(Board board)
        {
            BoardPo po = new BoardPo(board.Id, board.BoardName, new List<DomainEventPo>());
            impl.Save(po);
        }

        public override Board? FindById(string id)
        {
            BoardPo? po = (BoardPo) impl.FindById(id);
            return new Board(po.Id, po.BoardName);
        }
    }

}

