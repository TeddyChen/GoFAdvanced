﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;

namespace Tw.Teddysoft.Gof.Proxy.Exercise
{
    public class DescriptionNotFoundException : Exception
    {
    }
}
