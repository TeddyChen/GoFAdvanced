﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;
using NUnit.Framework;

namespace Tw.Teddysoft.Gof.Proxy.Ans
{
    [TestFixture]
    public class UIDescriptionManagerTest
    {
        [Test]
        public void testUseRealObjectToLoadUIDescription()
        {
            UIDescriptionManager manager = new UIDescriptionManagerImpl();
            DateTime start = DateTime.Now;
            manager.getDescription("ID_LIST_PRODUCT");
		    manager.getDescription("ID_LIST_PRODUCT");
		    manager.getDescription("ID_LIST_PRODUCT");
            DateTime end = DateTime.Now;
            TimeSpan ts = end - start;

            Console.WriteLine("start = " + start);
            Console.WriteLine("end = " + end);
            Console.WriteLine("ts.TotalMilliseconds = " + ts.TotalMilliseconds);


            Assert.True(6000 <= ts.TotalMilliseconds);
        }

        [Test]
        public void testUseProxyToLoadUIDescription() 
        {
            UIDescriptionManager manager = new UIDescriptionManagerProxy(new UIDescriptionManagerImpl());
            DateTime start = DateTime.Now;
            manager.getDescription("ID_LIST_PRODUCT");
		    manager.getDescription("ID_LIST_PRODUCT");
		    manager.getDescription("ID_LIST_PRODUCT");
            DateTime end = DateTime.Now;
            TimeSpan ts = end - start;

            Assert.True(3000 > ts.TotalMilliseconds);
        }
    }
}
