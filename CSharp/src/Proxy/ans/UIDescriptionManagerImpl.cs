﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;
using System.Threading;

namespace Tw.Teddysoft.Gof.Proxy.Ans
{
    public class UIDescriptionManagerImpl : UIDescriptionManager
    {
        public String getDescription(String aID)
        {
            return LoadDescriptionFromDatabase();
        }

        private String LoadDescriptionFromDatabase()
        {
            Console.WriteLine("Loading description from database needs 2 seconds...");
            sleepMillisecond(2000);
            return "";
        }

        private void sleepMillisecond(int ms)
        {
            Thread.Sleep(ms);
        }
    }
}