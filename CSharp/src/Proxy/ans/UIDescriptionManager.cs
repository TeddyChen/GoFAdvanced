﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;

namespace Tw.Teddysoft.Gof.Proxy.Ans
{
    public interface UIDescriptionManager
    {
        String getDescription(String aID);
    }
}
