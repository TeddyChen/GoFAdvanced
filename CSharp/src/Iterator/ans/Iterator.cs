﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;

namespace Tw.Teddysoft.Gof.Iterator.Ans
{
    public interface Iterator
    {
        Object next();
        bool hasNext();
    }
}
