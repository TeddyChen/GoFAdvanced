﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;
using System.Collections.Generic;

namespace Tw.Teddysoft.Gof.Iterator.Exercise
{
    public class BookList
    {
        private IList<Book> list;

        public BookList()
        {
            list = new List<Book>();
        }

        public void addBook(Book aBook)
        {
            list.Add(aBook);
        }

        public int size()
        {
            return list.Count;
        }

        public bool isEmpty()
        {
            return list.Count == 0;
        }

        public Book getBook(int aIndex)
        {
            return list[aIndex];
        }
    }

}
