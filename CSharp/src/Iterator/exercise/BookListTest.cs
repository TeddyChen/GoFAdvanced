﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;
using NUnit.Framework;
using System.Text;

namespace Tw.Teddysoft.Gof.Iterator.Exercise
{
    [TestFixture]
    public class BookListTest
    {
        [Test]
        public void testIterator()
        {
            BookList bookList = new BookList();
            bookList.addBook(new Book("笑談軟體工程", "Teddy", "悅之"));
            bookList.addBook(new Book("The Timeless Way of Building", "Christopher Alexander", "Oxford University Press"));
            bookList.addBook(new Book("Design Patterns", "GoF", "Addison-Wesley"));
            bookList.addBook(new Book("Object-Oriented Software Construction, 2nd", "Bertrand Meyer", "Prentice Hall"));
            bookList.addBook(new Book("Clean Code", "Robert C. Martin", "Prentice Hall"));

            String expected = "Title = 笑談軟體工程; Author = Teddy; Publisher = 悅之.Title = The Timeless Way of Building; Author = Christopher Alexander; Publisher = Oxford University Press.Title = Design Patterns; Author = GoF; Publisher = Addison-Wesley.Title = Object-Oriented Software Construction, 2nd; Author = Bertrand Meyer; Publisher = Prentice Hall.Title = Clean Code; Author = Robert C. Martin; Publisher = Prentice Hall.";

            StringBuilder sb = new StringBuilder();
            //TODO
            //Iterator iterator = bookList.createIterator();
            //while (iterator.hasNext())
            //{
            //    string item = iterator.next().ToString();
            //    Console.WriteLine(item);
            //    sb.Append(item);
            //}
            //Assert.AreEqual(expected, sb.ToString());
        }
    }
}
