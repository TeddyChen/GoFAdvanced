﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;
using System.Text;

namespace Tw.Teddysoft.Gof.Iterator.Exercise
{
    public class Book
    {
        private String title;
        private String author;
        private String publisher;

        public Book(String aTitle, String aAuthor,
                String aPublisher)
        {
            title = aTitle;
            author = aAuthor;
            publisher = aPublisher;
        }
        public String getTitle()
        {
            return title;
        }
        public String getAuthor()
        {
            return author;
        }
        public String getPublisher()
        {
            return publisher;
        }
        public override String ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Title = ").Append(title).Append("; ");
            sb.Append("Author = ").Append(author).Append("; ");
            sb.Append("Publisher = ").Append(publisher).Append(".");
            return sb.ToString();
        }
    }
}
