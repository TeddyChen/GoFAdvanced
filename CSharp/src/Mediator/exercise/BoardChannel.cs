﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;

namespace Tw.Teddysoft.Gof.Mediator.Exercise
{
    public interface BoardChannel
    {
        void Join(string boardId, Agent agent);

        void Left(string boardId, Agent agent);

        void BroadcastEvent(DomainEvent domainEvent, string boardId, Agent agent);
    }

}
