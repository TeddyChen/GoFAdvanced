﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;
using System.Runtime.Serialization;

namespace Tw.Teddysoft.Gof.Mediator.Exercise
{
    public record BoardEvents : DomainEvent
    {
        public record BoardCreated(
            string BoardId,
            string BoardName,
            string UserId,
            Guid Id,
            DateTime OccurredOn
            ) : CardEvents;
    }

}
