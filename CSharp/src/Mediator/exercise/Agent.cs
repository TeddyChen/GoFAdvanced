﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;

namespace Tw.Teddysoft.Gof.Mediator.Exercise
{
    public interface Agent
    {
        string GetId();
        void FireEvent(DomainEvent @event, string boardId);
    }

}
