﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;
using System.Runtime.Serialization;

namespace Tw.Teddysoft.Gof.Mediator.Exercise
{

    public record CardEvents : DomainEvent
    {
        public record CardMoved(
            string BoardId,
            string FromLaneId,
            string ToLaneId,
            string CardId,
            string UserId,
            Guid Id,
            DateTime OccurredOn
            ) : CardEvents;
    }

}
