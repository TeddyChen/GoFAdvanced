﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;
using NUnit.Framework;

namespace Tw.Teddysoft.Gof.Mediator.Ans
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Runtime.CompilerServices;
    using NUnit;

    namespace MediatorTest
    {
        [TestFixture]
        public class BoardChannelMediatorTest
        {
            [Test]
            public void TestMediator()
            {
                StringWriter output = new StringWriter();
                Console.SetOut(output);

                BoardChannel mediator = new BoardChannelMediator();
                var boardId = Guid.NewGuid().ToString();
                Agent teddy = new User("Teddy", boardId, mediator);
                Agent ada = new User("Ada", boardId, mediator);
                Agent eiffel = new User("Eiffel", boardId, mediator);
                Agent pascal = new User("Pascal", boardId, mediator);
                Agent chatPpt = new AiAgent("ChatPPT", boardId, mediator);


                teddy.FireEvent(new CardEvents.CardMoved(
                    "scrum_board-888",
                    "to_do",
                    "doing",
                    "card001",
                    teddy.GetId(),
                    Guid.NewGuid(),
                    DateTime.Now), boardId);

                teddy.FireEvent(new BoardEvents.BoardCreated(
                    "board id",
                    "Scrum Board",
                    teddy.GetId(),
                    Guid.NewGuid(),
                    DateTime.Now), boardId);

                var expected = "Ada received event from Teddy\nEiffel received event from Teddy\nPascal received event from Teddy\nChatPPT received event from Teddy";

                Assert.AreEqual(expected.Trim(), output.ToString().Trim());
            }
        }
    }

}
