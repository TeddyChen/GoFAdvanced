﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;

namespace Tw.Teddysoft.Gof.Mediator.Ans
{
    using System;
    using System.Collections.Generic;
    using static Tw.Teddysoft.Gof.Mediator.Ans.CardEvents;

    public class BoardChannelMediator : BoardChannel
    {

        private readonly Dictionary<string, List<Agent>> sessionMap;

        public BoardChannelMediator()
        {
            this.sessionMap = new Dictionary<string, List<Agent>>();
        }

        public void Join(string boardId, Agent agent)
        {
            if (!sessionMap.ContainsKey(boardId))
            {
                sessionMap.Add(boardId, new List<Agent>());
            }
            sessionMap[boardId].Add(agent);
        }

        public void Left(string boardId, Agent agent)
        {
            if (!sessionMap.ContainsKey(boardId))
            {
                return;
            }
            sessionMap[boardId].RemoveAll(x => x.GetId == agent.GetId);
        }

        public void BroadcastEvent(DomainEvent domainEvent, string boardId, Agent agent)
        {
            if (!sessionMap.ContainsKey(boardId)) return;
            CardMoved? cardEvent = domainEvent as CardMoved;
            if (cardEvent == null) return;

            foreach (var each in sessionMap[boardId])
            {
                switch (each)
                {
                    case User user:
                        if (user.GetId() != cardEvent.UserId)
                        {
                            user.ReceiveEvent(domainEvent);
                        }
                        break;
                    case AiAgent aiAgent:
                        aiAgent.LogEvent(domainEvent);
                        break;
                    default:
                        break;
                }
            }
        }
    }

}
