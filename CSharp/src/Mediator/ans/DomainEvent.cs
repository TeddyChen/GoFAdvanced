﻿/*
* Copyright 2017 TeddySoft Technology. 
* 
*/
using System;
using System.Runtime.Serialization;

namespace Tw.Teddysoft.Gof.Mediator.Ans
{
    public record DomainEvent 
    {
        Guid Id { get; }
        DateTime OccurredOn { get; }
        string AggregateId { get; }
    }

}
