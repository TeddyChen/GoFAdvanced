﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;
namespace Tw.Teddysoft.Gof.Builder.E2.Ans
{
    public interface ConfigPropertyBuilder
    {
        ConfigPropertyBuilder platform(String aValue);
        ConfigPropertyBuilder timeout(int aValue);
        ConfigPropertyBuilder location(String aPath);
        String build();
    }
}
