﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;
using NUnit.Framework;

namespace Tw.Teddysoft.Gof.Builder.E2.Ans
{
    [TestFixture]
    public class ConfigPropertyBuilderTest
    {
        [Test]
        public void testHowToUsePlainTextConfigPropertyBuilder() {
            String configString = PlainTextConfigPropertyBuilder.newBuilder()
                                            .timeout(30)
                                            .platform("Ubuntu")
                                            .location("\\opt\\property.txt")
                                            .build();

            Assert.AreEqual("TIMEOUT=30\nPLATFORM=Ubuntu\nLOCATION=\\opt\\property.txt\n", configString);
        }

        [Test]
        public void testPlainTextConfigPropertyBuilderLocationNotBeSetException() {
            try
            {
                String configString = PlainTextConfigPropertyBuilder.newBuilder()
                                        .timeout(30)
                                        .platform("Ubuntu")
                                        .build();

                    Assert.Fail("builder.build() should have thrown an exception");
                }
                catch (ConfigurationError e)
                {
                    Assert.AreEqual("The LOCATION property must be set.", e.Message);
                }
            }

        [Test]
        public void testHowToUseJsonConfigPropertyBuilder() {

            String configString = JsonConfigPropertyBuilder.newBuilder()
                .timeout(30)
                .platform("Ubuntu")
                .location("\\opt\\property.txt")
                .build();

            Assert.AreEqual("{\"PLATFORM\":\"Ubuntu\",\"TIMEOUT\":30,\"LOCATION\":\"\\opt\\property.txt\"}",
                        configString);
        }

        [Test]
        public void testJsonConfigPropertyBuilderLocationNotBeSetException()
        {
            try
            {
                String configString = JsonConfigPropertyBuilder.newBuilder()
                        .timeout(30)
                        .platform("Ubuntu")
                        .build();

                Assert.Fail("builder.build() should have thrown an exception");
            }
            catch (ConfigurationError e)
            {
                Assert.AreEqual("The LOCATION property must be set.", e.Message);
            }
        }
    }
}