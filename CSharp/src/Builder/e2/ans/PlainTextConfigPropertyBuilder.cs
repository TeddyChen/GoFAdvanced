﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;
using System.Collections.Generic;
using System.Text;

namespace Tw.Teddysoft.Gof.Builder.E2.Ans
{
    public class PlainTextConfigPropertyBuilder : ConfigPropertyBuilder
    {
        private Dictionary<String, String> _table;

        public PlainTextConfigPropertyBuilder()
        {
            _table = new Dictionary<String, String>();
        }

        public static ConfigPropertyBuilder newBuilder()
        {
            return new PlainTextConfigPropertyBuilder();
        }

        public ConfigPropertyBuilder platform(String aValue)
        {
            _table.Add("PLATFORM", aValue);
            return this;
        }

        public ConfigPropertyBuilder timeout(int aValue)
        {
            _table.Add("TIMEOUT", aValue.ToString());
            return this;
        }

        public ConfigPropertyBuilder location(String aPath)
        {
            _table.Add("LOCATION", aPath);
            return this;
        }

        public String build()
        {
            if (!_table.ContainsKey("LOCATION")) {
                throw new ConfigurationError("The LOCATION property must be set.");
            }

            StringBuilder sb = new StringBuilder();
            foreach (String s in _table.Keys) {
                sb.Append(s).Append("=").Append(_table[s]).Append("\n");
            }
            return sb.ToString();
        }
    }
}