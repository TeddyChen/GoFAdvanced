﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;
namespace Tw.Teddysoft.Gof.Builder.E2.Exercise
{
    public interface ConfigPropertyBuilder
    {
        void platform(String aValue);
        void timeout(int aValue);
        void location(String aPath);
        String build();
    }
}
