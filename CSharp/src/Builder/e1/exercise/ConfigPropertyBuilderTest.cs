﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;
using NUnit.Framework;

namespace Tw.Teddysoft.Gof.Builder.E1.Exercise
{
    [TestFixture]
    public class ConfigPropertyBuilderTest
    {
        [Test]
        public void testHowToUsePlainTextConfigPropertyBuilder() 
        {
            //TODO
      //      ConfigPropertyBuilder builder = new PlainTextConfigPropertyBuilder();
      //      builder.timeout(30);
		    //builder.platform("Ubuntu");
		    //builder.location("\\opt\\property.txt");

      //      Assert.AreEqual("TIMEOUT=30\nPLATFORM=Ubuntu\nLOCATION=\\opt\\property.txt\n", builder.build());
        }

        public void testHowToUseJsonConfigPropertyBuilder() 
        {
            //TODO
      //      ConfigPropertyBuilder builder = new JsonConfigPropertyBuilder();
      //      builder.timeout(30);
		    //builder.platform("Ubuntu");
		    //builder.location("\\opt\\property.txt");

      //      Assert.AreEqual("{\"PLATFORM\":\"Ubuntu\",\"TIMEOUT\":30,\"LOCATION\":\"\\opt\\property.txt\"}",
      //              builder.build());
        }

        public void testPlainTextConfigPropertyBuilderLocationNotBeSetException()
        {
            //TODO
            //try
            //{
            //    ConfigPropertyBuilder builder = new PlainTextConfigPropertyBuilder();
            //    builder.timeout(30);
            //    builder.platform("Ubuntu");
            //    builder.build();

            //    Assert.Fail("builder.build() should have thrown an exception");
            //}
            //catch (ConfigurationError e)
            //{
            //        Assert.AreEqual("The LOCATION property must be set.", e.Message);
            //}
        }

        public void testJsonConfigPropertyBuilderLocationNotBeSetException()
        {
            //TODO
            //try
            //{
            //    ConfigPropertyBuilder builder = new JsonConfigPropertyBuilder();
            //    builder.timeout(30);
            //    builder.platform("Ubuntu");
            //    builder.build();

            //    Assert.Fail("builder.build() should have thrown an exception");
            //}
            //catch (ConfigurationError e)
            //{
            //    Assert.AreEqual("The LOCATION property must be set.", e.Message);
            //}
        }
    }
}
