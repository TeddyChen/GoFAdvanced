﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;
using System.Collections.Generic;
using System.Text;

namespace Tw.Teddysoft.Gof.Builder.E1.Exercise
{
    public class ConfigProperty
    {
        private Dictionary<String, String> _table = new Dictionary<String, String>();

        public void put(String aKey, String aValue)
        {
            _table.Add(aKey, aValue);
        }

        public String get(String aKey)
        {
            return _table[aKey];
        }

        public String toString()
        {
            StringBuilder sb = new StringBuilder();

            foreach (String s in _table.Keys)
            {
                sb.Append(s).Append("=").Append(_table[s]).Append("\n");
            }
            return sb.ToString();
        }
    }
}
