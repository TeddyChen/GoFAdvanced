﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;
namespace Tw.Teddysoft.Gof.Builder.E1.Ans
{
    public class ConfigurationError : Exception
    {
        public ConfigurationError() : base()
        {
        }

        public ConfigurationError(String aStr) : base(aStr)
        {
        }
    }
}



