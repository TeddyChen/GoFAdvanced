﻿/*
 * Copyright Teddysoft Technology. 
 * 
 */
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Tw.Teddysoft.Gof.Memento.Exercise.Entity
{
    public class Card : AggregateRoot, AggregateSnapshot<Card.CardSnapshot>
    {
        private string laneId;
        private string description;
        private List<string> assignees;
        private DateTime? deadline = null;


        public CardSnapshot GetSnapshot()
        {
            return new CardSnapshot(laneId, Id, Description, Assignees, Deadline, Version);
        }

        public void SetSnapshot(CardSnapshot snapshot)
        {
            laneId = snapshot.LaneId;
            id = snapshot.CardId;
            description = snapshot.Description;
            assignees = snapshot.Assignees;
            deadline = snapshot.Deadline;
            version = snapshot.Version;
        }

        public record CardSnapshot(
            string LaneId,
            string CardId,
            string Description,
            List<string> Assignees,
            DateTime? Deadline,
            long Version
        );

        public static Card FromSnapshot(CardSnapshot snapshot)
        {
            Card card = new Card();
            card.SetSnapshot(snapshot);
            return card;
        }

        protected Card(): base() { }

        public Card(List<DomainEvent> domainEvents) : base(domainEvents) { }

        public Card(string laneId, string cardId, string description) : base(cardId)
        {
            Apply(new CardEvents.CardCreated(
                laneId,
                cardId,
                description,
                deadline,
                Guid.NewGuid(),
                DateTime.Now
            ));
        }

        public void ChangeDescription(string newDescription)
        {
            Apply(new CardEvents.CardDescriptionChanged(
                Id,
                newDescription,
                Guid.NewGuid(),
                DateTime.Now
            ));
        }

        public void ChangeDeadline(DateTime newDeadline)
        {
            Apply(new CardEvents.CardDeadlineChanged(
                Id,
                newDeadline,
                Guid.NewGuid(),
                DateTime.Now
            ));
        }

        public string Description => description;

        public List<string> Assignees => assignees;

        public void Assign(string assignee)
        {
            Apply(new CardEvents.CardAssigned(
                id,
                assignee,
                Guid.NewGuid(),
                DateTime.Now
            ));
        }

        public void Unassign(string unassignee)
        {
            Apply(new CardEvents.CardUnassigned(
                id,
                unassignee,
                Guid.NewGuid(),
                DateTime.Now
            ));
        }

        public DateTime? Deadline => deadline;

        protected override void When(DomainEvent @event)
        {
            switch (@event)
            {
                case CardEvents.CardCreated e:
                    laneId = e.LaneId;
                    id = e.CardId;
                    description = e.Description;
                    deadline = e.Deadline;
                    assignees = new List<string>();
                    break;
                case CardEvents.CardDescriptionChanged e:
                    description = e.Description;
                    break;
                case CardEvents.CardAssigned e:
                    assignees.Add(e.Assignee);
                    break;
                case CardEvents.CardUnassigned e:
                    assignees.Remove(e.Unassignee);
                    break;
                case CardEvents.CardDeadlineChanged e:
                    deadline = e.Deadline;
                    break;
            }
        }
    }
}
