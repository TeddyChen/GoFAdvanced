﻿/*
 * Copyright Teddysoft Technology. 
 * 
 */
using System;
using Tw.Teddysoft.Gof.Memento.Exercise.Entity;

namespace Tw.Teddysoft.Gof.Memento.Exercise.Usecase
{
    public interface Repository<T> where T : AggregateRoot
    {
        T? FindById(string id);
        void Save(T entity);
        void Delete(T entity);
    }
}
