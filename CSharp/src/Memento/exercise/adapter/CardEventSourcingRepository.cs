﻿/*
 * Copyright Teddysoft Technology. 
 * 
 */
using System;
using Newtonsoft.Json;
using System.Runtime.CompilerServices;
using Tw.Teddysoft.Gof.Memento.Exercise.Entity;
using Tw.Teddysoft.Gof.Memento.Exercise.Usecase;

namespace Tw.Teddysoft.Gof.Memento.Exercise.Adapter
{
    public class CardEventSourcingRepository : Repository<Card>
    {
        private readonly CardStore cardStore;

        public CardEventSourcingRepository(CardStore cardStore)
        {
            this.cardStore = cardStore;
        }

        public void Save(Card card)
        {
            cardStore.Save(card);
            card.ClearDomainEvents();
        }

        public Card? FindById(string cardId)
        {
            return cardStore.FindById(cardId);
        }

        public void Delete(Card card)
        {
            cardStore.Delete(card);
        }
    }
}
