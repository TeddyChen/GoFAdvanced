﻿/*
 * Copyright Teddysoft Technology. 
 * 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using Tw.Teddysoft.Gof.Memento.Exercise.Entity;

namespace Tw.Teddysoft.Gof.Memento.Exercise.Adapter
{
    public class CardStore
    {
        private Dictionary<string, List<DomainEvent>> stores = new Dictionary<string, List<DomainEvent>>();

        public void Save(Card entity)
        {
            if (!stores.ContainsKey(entity.Id))
            {
                stores[entity.Id] = new List<DomainEvent>();
            }

            long version = stores[entity.Id].Count + entity.GetDomainEvents().Count;
            stores[entity.Id].AddRange(entity.GetDomainEvents());
            entity.Version = version;
        }

        public void Delete(Card entity)
        {
            this.Save(entity);
        }

        public Card? FindById(string id)
        {
            if (!stores.ContainsKey(id))
            {
                return default;
            }

            List<DomainEvent> events = stores[id];
            if (!events.Any())
            {
                return default;
            }

            Card card = new Card(events);
            return card;
        }

        public DomainEvent? GetLastEventFromStream(string streamName)
        {
            if (!stores.ContainsKey(streamName))
            {
                return default;
            }

            List<DomainEvent> events = stores[streamName];
            if (!events.Any())
            {
                return default;
            }

            return events.Last();
        }

        public void SaveEvent(string streamName, DomainEvent @event)
        {
            if (!stores.ContainsKey(streamName))
            {
                stores[streamName] = new List<DomainEvent>();
            }

            stores[streamName].Add(@event);
        }

        public List<DomainEvent> GetEventFromStream(string streamName, long version)
        {
            if (!stores.ContainsKey(streamName))
            {
                return new List<DomainEvent>();
            }

            List<DomainEvent> events = stores[streamName];
            if (!events.Any())
            {
                return events;
            }

            return events.Skip((int)version).ToList();
        }
    }

}
