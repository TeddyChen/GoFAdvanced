﻿/*
 * Copyright Teddysoft Technology. 
 * 
 */
using System;
using System.Runtime.CompilerServices;

namespace Tw.Teddysoft.Gof.Memento.Ans.Entity
{
    public interface AggregateSnapshot<T>
    {
        T GetSnapshot();
        void SetSnapshot(T snapshot);
    }

}
