﻿/*
 * Copyright Teddysoft Technology. 
 * 
 */
using System;
using NUnit.Framework;
using Tw.Teddysoft.Gof.Memento.Ans.Entity;
using Tw.Teddysoft.Gof.Memento.Ans.Adapter;
using Tw.Teddysoft.Gof.Memento.Ans.Usecase;

namespace Tw.Teddysoft.Gof.Memento.Ans
{

    [TestFixture]
    public class MementoTest
    {
        [Test]
        public void GenerateCardSnapshotAndReadCardFromIt()
        {
            CardStore cardStore = new CardStore();
            Repository<Card> cardRepository = new CardEventSourcingRepository(cardStore);

            Card card = new Card("lane-001", Guid.NewGuid().ToString(), "my card");
            card.Assign("Teddy");
            card.Assign("Eiffel");
            card.Assign("Ada");
            card.Assign("Pascal");
            card.ChangeDeadline(DateTime.Now);
            cardRepository.Save(card);
            card.ChangeDescription("new card description");
            card.Unassign("Teddy");
            cardRepository.Save(card);

            var storedCard = cardRepository.FindById(card.Id);
            Assert.AreEqual(card.Description, storedCard.Description);
            Assert.AreEqual(card.Assignees.Count, storedCard.Assignees.Count);

            var snapshotted = (Snapshotted)cardStore.GetLastEventFromStream(
                CardEventSourcingRepository.GetSnapshottedStreamName(card.Id));
            Console.WriteLine(snapshotted.Snapshot);
        }
    }
}
