﻿/*
 * Copyright Teddysoft Technology. 
 * 
 */
using System;
using System.Runtime.CompilerServices;

namespace Tw.Teddysoft.Gof.Memento.Ans
{
    public class Player
    {
        private int hp = 100;
        private int ep = 100;
        private double internalFactor = 0.10;

        public class Memento
        {
            private int _hp;
            private int _ep;
            private double _internalFactor;

            public Memento(int aHP, int aEP, double aFactor)
            {
                _hp = aHP;
                _ep = aEP;
                _internalFactor = aFactor;
            }

            public int getHP()
            {
                return _hp;
            }

            public int getEP()
            {
                return _ep;
            }

            internal double getInternalFactor()
            {
                return _internalFactor;
            }
        }

        public Memento createMemento()
        {
            return new Memento(hp, ep, internalFactor);
        }

        public void setMemento(Memento aMemento)
        {
            hp = aMemento.getHP();
            ep = aMemento.getEP();
            internalFactor = aMemento.getInternalFactor();
        }

        public void displayState()
        {
            Console.WriteLine("玩家狀態");
            Console.WriteLine("生命力 : " + hp);
            Console.WriteLine("經驗值 : " + ep);
            Console.WriteLine("內部因子 : " + internalFactor);
        }

        public void setHP(int aHP)
        {
            hp = aHP;
        }

        public void setEP(int aEP)
        {
            ep = aEP;
        }

        public int getHP()
        {
            return hp;
        }

        public int getEP()
        {
            return ep;
        }

        public void fight()
        {
            hp = hp - 10;
            ep = ep + 5;
            internalFactor = internalFactor + 0.008;
        }

        public void fightToDead()
        {
            hp = 0;
            ep = 0;
            internalFactor = 0;
        }
    }
}
