package tw.teddysoft.gof.mediator.ans;

public interface Agent {
    String getId();
    void fireEvent(DomainEvent event, String boardId);
}
