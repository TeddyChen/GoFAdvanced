package tw.teddysoft.gof.mediator.exercise;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

public interface DomainEvent extends Serializable {
    UUID id();
    Date occurredOn();
    String aggregateId();
}
