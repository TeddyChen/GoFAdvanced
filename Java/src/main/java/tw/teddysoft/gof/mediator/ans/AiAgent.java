package tw.teddysoft.gof.mediator.ans;

import static java.lang.String.format;

public class AiAgent implements Agent {
    private String id;
    private BoardChannel notifyBoardChannel;

    public AiAgent(String id, String boardId, BoardChannel notifyBoardChannel) {
        this.id = id;
        this.notifyBoardChannel = notifyBoardChannel;
        this.notifyBoardChannel.join(boardId, this);
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void fireEvent(DomainEvent event, String boardId) {
        notifyBoardChannel.broadcastEvent(event, boardId, this);
    }

    public void logEvent(DomainEvent event){
            switch (event) {
                case CardEvents.CardMoved e -> {
                    System.out.println(format("%s received event from %s", id, e.userId()));
                }
                default -> {
                }
            }
    }
}
