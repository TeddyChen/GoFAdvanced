package tw.teddysoft.gof.mediator.ans;


public interface BoardChannel {

    void join(String boardId, Agent agent);

    void left(String boardId, Agent agent);

    void broadcastEvent(DomainEvent event, String boardId, Agent agent);
}
