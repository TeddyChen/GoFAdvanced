package tw.teddysoft.gof.mediator.ans;

import java.util.Date;
import java.util.UUID;

public interface BoardEvents extends DomainEvent {

    String boardId();

    String userId();

    default String aggregateId(){
        return boardId();
    }

    ///////////////////////////////////////////////////////////////

    record BoardCreated(
            String boardId,
            String boardName,
            String userId,
            UUID id,
            Date occurredOn
    ) implements BoardEvents {}

    ///////////////////////////////////////////////////////////////

 }
