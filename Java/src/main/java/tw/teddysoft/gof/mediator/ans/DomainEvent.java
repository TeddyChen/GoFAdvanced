package tw.teddysoft.gof.mediator.ans;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

public interface DomainEvent extends Serializable {
    UUID id();
    Date occurredOn();
    String aggregateId();
}
