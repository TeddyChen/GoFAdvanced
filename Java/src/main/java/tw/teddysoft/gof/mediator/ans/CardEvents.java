package tw.teddysoft.gof.mediator.ans;

import java.util.Date;
import java.util.UUID;

public interface CardEvents extends DomainEvent {

    String cardId();

    String userId();

    default String aggregateId(){
        return cardId();
    }

    ///////////////////////////////////////////////////////////////

    record CardMoved(
            String boardId,
            String fromLneId,
            String toLaneId,
            String cardId,
            String userId,
            UUID id,
            Date occurredOn
    ) implements CardEvents {}

    ///////////////////////////////////////////////////////////////

 }
