package tw.teddysoft.gof.mediator.exercise;

public interface Agent {
    String getId();
    void fireEvent(DomainEvent event, String boardId);
}
