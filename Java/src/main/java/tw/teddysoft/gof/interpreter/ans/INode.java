/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.interpreter.ans;

public interface INode {
	
	void parse(IContext context);
	void execute();
}
