/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.interpreter.ans;

/*
 * <command> ::= <block> | <primitive>
 */
public class Command implements INode {

	private INode _node;
	
	@Override
	public void parse(IContext context) {
		if(context.currentToken().equals("BLOCK")){
			_node = new Block();
			_node.parse(context);
		}
		else{
			_node = new Primitive();
			_node.parse(context);
		}
	}

	@Override
	public void execute() {
		// Do not execute Block unless someone CALLs the block
		if (_node instanceof Primitive)
			_node.execute();
	}
}
