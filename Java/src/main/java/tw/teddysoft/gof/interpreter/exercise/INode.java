/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.interpreter.exercise;

public interface INode {
	
	void parse(IContext context);
	void execute();
}
