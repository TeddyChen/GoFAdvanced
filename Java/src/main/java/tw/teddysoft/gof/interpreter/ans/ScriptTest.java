/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.interpreter.ans;

import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;


public class ScriptTest {

	@Test
	public void testSimpleScript() throws FileNotFoundException {
		INode script = new Script();
		script.parse(new Context("./etc/simple.txt"));
		script.execute();
	}

	@Test
	public void testBlockScript() throws FileNotFoundException {
		INode script = new Script();
		script.parse(new Context("./etc/block.txt"));
		script.execute();
	}

	@Test
	public void testNestedBlockScript() throws FileNotFoundException {
		INode script = new Script();
		script.parse(new Context("./etc/nestedblock.txt"));
		script.execute();
	}

	@Test
	public void testInvalidBlockID() throws FileNotFoundException {
		INode script = new Script();
		script.parse(new Context("./etc/invalidBlockID.txt"));
		script.execute();
	}
	
}
