package tw.teddysoft.gof.visitor.ans.visitor;

public interface SpecificationElementVisitor {
    void visit (SpecificationElement element);
}
