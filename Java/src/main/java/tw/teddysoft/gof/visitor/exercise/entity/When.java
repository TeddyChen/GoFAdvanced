package tw.teddysoft.gof.visitor.exercise.entity;

public class When extends Step {

    public When(String description, Runnable callback){
        super(description, callback);
    }

    @Override
    public String getName() {
        return "When";
    }
}
