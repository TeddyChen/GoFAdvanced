package tw.teddysoft.gof.visitor.ans.visitor;

public interface SpecificationElement {
    String getName();
    void accept(SpecificationElementVisitor visitor);
}
