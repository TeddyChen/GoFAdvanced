package tw.teddysoft.gof.visitor.exercise.entity;

public class Given extends Step {
    public Given(String description, Runnable callback){
        super(description, callback);
    }

    @Override
    public String getName() {
        return "Given";
    }
}
