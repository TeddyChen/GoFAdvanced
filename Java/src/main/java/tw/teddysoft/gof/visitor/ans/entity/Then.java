package tw.teddysoft.gof.visitor.ans.entity;

public class Then extends Step {

    public Then(String description, Runnable callback){
        super(description, callback);
    }

    @Override
    public String getName() {
        return "Then";
    }
}
