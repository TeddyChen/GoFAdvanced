package tw.teddysoft.gof.visitor.exercise.entity;

import tw.teddysoft.gof.visitor.exercise.visitor.SpecificationElement;
import tw.teddysoft.gof.visitor.exercise.visitor.SpecificationElementVisitor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Scenario {
    public static final String KEYWORD = "Scenario";
    private final String name;
    private final List<Step> steps;

    public Scenario(String name){
        this.name = name;
        steps = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public Scenario Given(String description, Runnable callback){
        var step = new Given(description, callback);
        getSteps().add(step);
        return this;
    }
    public Scenario When(String description, Runnable callback){
        var step = new When(description, callback);
        getSteps().add(step);
        return this;
    }
    public Scenario Then(String description, Runnable callback){
        var step = new Then(description, callback);
        getSteps().add(step);
        return this;
    }
    public Scenario And(String description, Runnable callback){
        var step = new And(description, callback);
        getSteps().add(step);
        return this;
    }

    public List<Step> steps(){
        return Collections.unmodifiableList(steps);
    }

    protected List<Step> getSteps() {
        return steps;
    }

    public String fullText(){
        StringBuilder sb = new StringBuilder();

        sb.append("Scenario: ").append(getName()).append("\n");
        for(var each : getSteps()){
            sb.append(each.getName());
            if (!each.description().isEmpty()){
                sb.append(" ").append(each.description());
            }
            sb.append("\n");
        }
        return sb.toString();
    }

    protected void executeStep(Scenario self, Step step){
        try{
            step.getCallback().run();
            step.setResult(Result.Successful());
        }
        catch (Throwable e) {
            step.setResult(Result.Failure(e));
            throw e;
        }
    }

    public void Execute() {
        for (int i = 0; i < getSteps().size(); i++) {
            try {
                executeStep(this, getSteps().get(i));
            } catch (Throwable e) {
                for (int k = i + 1; k < getSteps().size(); k++) {
                    getSteps().get(k).setResult(Result.Skip());
                }
                throw e;
            }
        }
    }
}
