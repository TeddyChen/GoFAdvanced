package tw.teddysoft.gof.visitor.ans.visitor;

import tw.teddysoft.gof.visitor.ans.entity.Feature;
import tw.teddysoft.gof.visitor.ans.entity.Scenario;
import tw.teddysoft.gof.visitor.ans.entity.Step;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import static java.lang.String.format;

public class PlainTextVisitor implements SpecificationElementVisitor {
    private final StringBuilder output = new StringBuilder();

    @Override
    public void visit(SpecificationElement element) {
        switch (element){
            case Feature feature -> {
                output.append(feature.featureText());
            }
            case Scenario scenario ->  {
                output.append(format("\n\n%s", scenario.fullText()));
            }
            case Step step -> {
                output.append(format("\n[%s] %s %s",
                        step.getResult(), step.getName(), step.description()));
            }
            default -> {}
        }
    }

    public String getOutput() {
        return output.toString();
    }

    public void writeToFile(String fileName){
        try (FileWriter fileWriter = new FileWriter(fileName);
             PrintWriter printWriter = new PrintWriter(fileWriter)) {
            printWriter.print(output);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
