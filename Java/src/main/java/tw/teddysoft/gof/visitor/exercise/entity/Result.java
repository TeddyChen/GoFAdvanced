package tw.teddysoft.gof.visitor.exercise.entity;

public class Result {

    private StepExecutionOutcome executionOutcome;
    private Throwable error;

    public static Result Successful(){
        return new Result(StepExecutionOutcome.Success, null);
    }
    public static Result Failure(Throwable error){
        return new Result(StepExecutionOutcome.Failure, error);
    }
    public static Result Skip() {
        return new Result(StepExecutionOutcome.Skipped, null);
    }
    public static Result Pending(Throwable error) {
        return new Result(StepExecutionOutcome.Pending, error);
    }

    private Result(StepExecutionOutcome outcome, Throwable error) {
        this.executionOutcome = outcome;
        this.error = error;
    }

    public String toString(){
        return executionOutcome.name();
    }

    public Throwable getException(){
        return error;
    }

}