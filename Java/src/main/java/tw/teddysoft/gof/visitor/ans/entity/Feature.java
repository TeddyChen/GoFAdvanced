package tw.teddysoft.gof.visitor.ans.entity;

import tw.teddysoft.gof.visitor.ans.visitor.SpecificationElement;
import tw.teddysoft.gof.visitor.ans.visitor.SpecificationElementVisitor;

import java.util.LinkedList;
import java.util.List;

public class Feature implements SpecificationElement {

    public static final String KEYWORD = "Feature";
    private final String name;
    private final String description;
    private final List<Scenario> scenarios;

    public static Feature New(String name){
        return new Feature(name);
    }

    public static Feature New(String name, String description){
        return new Feature(name, description);
    }

    public Feature(String name){
        this(name, "");
    }

    public Feature(String name, String description){
        this.name = name;
        this.description = description;
        scenarios = new LinkedList<>();
    }

    public List<Scenario> getScenarios() {
        return scenarios;
    }

    public String getName(){
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Scenario newScenario(String name) {
        var scenario = new Scenario(name);
        scenarios.add(scenario);
        return scenario;
    }

    public String featureText() {
        StringBuilder sb = new StringBuilder();
        sb.append("Feature: ").append(name);

        if (!description.isEmpty())
            sb.append("\n\n").append(description);

        return sb.toString();
    }

    private String scenariosText() {
        StringBuilder sb = new StringBuilder();
        for(var each : scenarios){
            sb.append(each.fullText()).append("\n");
        }
        return sb.toString();
    }

    public String fullText() {
        StringBuilder sb = new StringBuilder(featureText());
        sb.append(scenariosText());
        return sb.toString();
    }

    @Override
    public void accept(SpecificationElementVisitor visitor) {
        visitor.visit(this);
        scenarios.forEach(scenario -> {
            scenario.accept(visitor);
        });
    }
}
