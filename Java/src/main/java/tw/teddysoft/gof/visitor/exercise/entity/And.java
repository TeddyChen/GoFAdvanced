package tw.teddysoft.gof.visitor.exercise.entity;

public class And extends Step {
    public And(String description, Runnable callback){
        super(description, callback);
    }

    @Override
    public String getName() {
        return "And";
    }
}
