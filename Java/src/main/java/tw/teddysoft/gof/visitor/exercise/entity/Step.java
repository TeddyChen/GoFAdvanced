package tw.teddysoft.gof.visitor.exercise.entity;

import tw.teddysoft.gof.visitor.exercise.visitor.SpecificationElement;
import tw.teddysoft.gof.visitor.exercise.visitor.SpecificationElementVisitor;


public abstract class Step {
    private String description;
    private Result result;
    private Runnable callback;

    public Step(String description, Runnable callback){
        this.description = description;
        this.callback = callback;
    }

    public Runnable getCallback() {
        return callback;
    }

    public String description(){
        return description;
    }

    public Result getResult() {
        return result;
    }


    public void setResult(Result result) {
        this.result = result;
    }

    public abstract String getName();

}
