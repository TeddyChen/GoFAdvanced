package tw.teddysoft.gof.visitor.exercise.entity;

public enum StepExecutionOutcome {
    Pending, Success, Failure, Skipped;
}
