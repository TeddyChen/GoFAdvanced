///*
// * Copyright 2016 TeddySoft Technology. All rights reserved.
// *
// */
//package tw.teddysoft.gof.flyweight.exercise;
//
//import java.util.LinkedList;
//
//import org.junit.jupiter.api.Test;
//import tw.teddysoft.gof.flyweight.ans.ExteriorFactory;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//
//public class MonsterTest {
//
//	@Test
//	public void testTheNumberOfFlyweightEqualsTheKindsOfUsedMonster() {
//		int max = 100;
//		List<Monster> momsters = new LinkedList<>();
//		for(int i = 0; i < max; i++){
//			momsters.add(new Monster("蜘蛛精", 50, 80, "./modela.xml"));
//		}
//		for(int i = 0; i < max; i++){
//			momsters.add(new Monster("白骨精", 40, 70, "./modelb.xml"));
//		}
//
//		assertEquals(200, momsters.size());
//		assertEquals(2, ExteriorFactory.getInstance().getFlyweightSize());
//	}
//}
