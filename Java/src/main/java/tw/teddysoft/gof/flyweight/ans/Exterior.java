/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.flyweight.ans;

import java.awt.Color;

import javax.swing.text.Style;


public interface Exterior {

	void draw(Color aColor, Style aStyle);
}
