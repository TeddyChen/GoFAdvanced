/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.flyweight.exercise;

import java.awt.Color;
import javax.swing.text.Style;


public class Exterior {
	private String _source = null;
	private ThreeDModel _model = null;
	
	private Color _color = null;
	private Style _style = null;
	
	public void draw(){
		/* 
		  use the 3D model from the _source, 
		  Color, and Style to show the 3D model on the screen.		
		*/
	}
	
	
	public Exterior clone(){
		Exterior newObject = null;
		try {
			newObject = (Exterior)super.clone();
		} catch (CloneNotSupportedException e) {
			throw null;	// ignoring, should not happen
		}
		
		memoryCopy(this, newObject);

		return newObject;
	}
	
	public Exterior(String aFileName){
		_source = aFileName;
		
		// loading exterior from the file
		try {
			Thread.sleep(20);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} // sleep 20 ms
		
	}
	
	public String getSource(){
		return _source;
	}
	
	private void memoryCopy(Exterior aSrource, Exterior aDest){

		// sleep 5 ms
		try {
			Thread.sleep(5);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
}
