/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.flyweight.ans;

import java.awt.Color;

import javax.swing.text.Style;

public class SharedExterior implements Exterior {

	public String _source = null;
	public ThreeDModel _model = null;
	
	public void draw(Color aColor, Style aStyle){
		
		/* 
		  use the 3D model from the _source, 
		  the Color, and the Style arguments to show 
		  the 3D model on the screen.		
		*/
	}
	
	
	public SharedExterior(String aFileName){
		_source = aFileName;
		
		// loading exterior from the file
		try {
			Thread.sleep(20);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} // sleep 20 ms
		
	}
	
	public String getSource(){
		return _source;
	}
	
}
