/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.flyweight.ans;

import java.awt.Color;

import javax.swing.text.Style;


public class Monster {
	private int _hp = 100;	
	private int _power = 50;
	private String _name; 
	private Exterior _exterior = null;
	
	private Color _color = null;
	private Style _style = null;
	
	public Monster (String aName, int _hp, int _power, String aFileName){
		this._name = aName;
		this._hp = _hp;
		this._power = _power;

		this._exterior =  ExteriorFactory.getInstance().getFlyweight(aFileName);
	}
	
	public Monster cloneShallow(){
		Monster newObject = null;
		try {
			newObject = (Monster)super.clone();
		} catch (CloneNotSupportedException e) {
			throw null; 	// ignoring, should not happen
		}
		newObject._name = this._name;
		newObject._hp = this._hp;
		newObject._power = this._power;	
		newObject._exterior = this._exterior;
		return newObject;
	}
	
	public int getHP(){
		return _hp;
	}
	
	public int getPower(){
		return _power;
	}
	
	public Exterior getExterior(){
		return _exterior;
	}
	
	public void display(){
		System.out.println("Name: " + _name + ", HP: " + _hp + ", Power: " + _power + "\n");
	}
	
	public void hit(){
		this._hp -= 10;
		this._power += 5;
	}
}
