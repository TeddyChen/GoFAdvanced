package tw.teddysoft.gof.bridge.ans.entity;

import java.util.ArrayList;
import java.util.List;

public abstract class AggregateRoot {
    private String id;
    protected List<DomainEvent> domainEvents;

    public AggregateRoot(String id){
        this.id = id;
        domainEvents = new ArrayList<>();
    }
    public String getId(){
        return id;
    }
    public void addDomainEvent(DomainEvent event){
        domainEvents.add(event);
    }
    public List<DomainEvent> getDomainEvents(){
        return domainEvents;
    }
    public void clearDomainEvents(){
        domainEvents.clear();
    }
}
