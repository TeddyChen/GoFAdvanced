package tw.teddysoft.gof.bridge.ans.adapter;

import tw.teddysoft.gof.bridge.ans.entity.Card;
import tw.teddysoft.gof.bridge.ans.usecase.CardPo;
import tw.teddysoft.gof.bridge.ans.usecase.Repository;
import tw.teddysoft.gof.bridge.ans.usecase.RepositoryImpl;

import java.util.ArrayList;
import java.util.Optional;

public class CardRepository extends Repository<Card> {
    public CardRepository(RepositoryImpl implementation) {
        super(implementation);
    }
    @Override
    public void save(Card card) {
        CardPo po = new CardPo(card.getId(), card.getCardName(), card.getAssignee(), new ArrayList<>());
        impl.save(po);
    }

    @Override
    public Optional<Card> findById(String id) {
        Optional<CardPo> cardPo = impl.findById(id);
        return cardPo.map(po -> new Card(po.getId(), po.getName(), po.getAssignee()));
    }
}
