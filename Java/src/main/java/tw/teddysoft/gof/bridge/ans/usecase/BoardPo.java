package tw.teddysoft.gof.bridge.ans.usecase;

import java.util.List;

public class BoardPo implements PersistentObject {
    private String id;
    private String boardName;
    private List<DomainEventPo> domainEventPos;
    public BoardPo(String boardId, String boardName,
                   List<DomainEventPo> domainEventPos) {
        this.id = boardId;
        this.boardName = boardName;
        this.domainEventPos = domainEventPos;
    }
    @Override
    public String getId() {
        return id;
    }
    @Override
    public List<DomainEventPo> getDomainEvents() {
        return domainEventPos;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getBoardName() {
        return boardName;
    }
    public void setBoardName(String boardName) {
        this.boardName = boardName;
    }
}
