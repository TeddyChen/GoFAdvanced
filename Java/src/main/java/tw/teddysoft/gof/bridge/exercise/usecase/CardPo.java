package tw.teddysoft.gof.bridge.exercise.usecase;

import java.util.List;

public class CardPo implements PersistentObject {

    private String id;
    private String name;

    private String assignee;

    private List<DomainEventPo> domainEventPos;

    public CardPo(String boardId, String name, String assignee, List<DomainEventPo> domainEventPos) {
        this.id = boardId;
        this.name = name;
        this.assignee = assignee;
        this.domainEventPos = domainEventPos;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public List<DomainEventPo> getDomainEvents() {
        return domainEventPos;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }
}
