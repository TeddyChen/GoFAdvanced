/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.bridge.exercise;

import org.junit.jupiter.api.Test;
import tw.teddysoft.gof.bridge.exercise.usecase.Repository;
import tw.teddysoft.gof.bridge.exercise.adapter.BoardRepository;
import tw.teddysoft.gof.bridge.exercise.adapter.FakeMySQLRepositoryImpl;
import tw.teddysoft.gof.bridge.exercise.adapter.InMemoryRepositoryImpl;
import tw.teddysoft.gof.bridge.exercise.entity.Board;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BridgeTest {

	@Test
	public void testInMemoryStateSourcingBoardRepository(){

		Repository<Board> boardRepository = new BoardRepository(new InMemoryRepositoryImpl());
		String boardId = UUID.randomUUID().toString();
		Board board = new Board(boardId, "Scrum");

		boardRepository.save(board);

		Board storedBoard = boardRepository.findById(boardId).get();
		assertEquals(boardId, storedBoard.getId());
		assertEquals("Scrum", storedBoard.getBoardName());
	}

	@Test
	public void testFakeMySqlEventSourcingBoardRepository(){

		Repository<Board> boardRepository = new BoardRepository(new FakeMySQLRepositoryImpl());
		String boardId = UUID.randomUUID().toString();
		Board board = new Board(boardId, "Scrum");

		boardRepository.save(board);

		Board storedBoard = boardRepository.findById(boardId).get();
		assertEquals(boardId, storedBoard.getId());
		assertEquals("Scrum", storedBoard.getBoardName());
	}

//	@Test
//	public void testInMemoryStateSourcingCardRepository(){
//
//		Repository<Card> cardRepository = new CardRepository(new InMemoryStateSourcingRepositoryImpl());
//		String cardId = UUID.randomUUID().toString();
//		Card card = new Card(cardId, "Implement bridge pattern", "Teddy");
//
//		cardRepository.save(card);
//
//		Card storedCard = cardRepository.findById(cardId).get();
//		assertEquals(cardId, storedCard.getId());
//		assertEquals("Implement bridge pattern", storedCard.getCardName());
//		assertEquals("Teddy", storedCard.getAssignee());
//	}
//
//	@Test
//	public void testFakeMySqlEventSourcingCardRepository(){
//
//		Repository<Card> cardRepository = new CardRepository(new FakeMySQLStateSourcingRepositoryImpl());
//		String cardId = UUID.randomUUID().toString();
//		Card card = new Card(cardId, "Implement bridge pattern", "Teddy");
//
//		cardRepository.save(card);
//
//		Card storedCard = cardRepository.findById(cardId).get();
//		assertEquals(cardId, storedCard.getId());
//		assertEquals("Implement bridge pattern", storedCard.getCardName());
//		assertEquals("Teddy", storedCard.getAssignee());
//	}

}
