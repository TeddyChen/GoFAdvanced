package tw.teddysoft.gof.bridge.ans.usecase;

import tw.teddysoft.gof.bridge.ans.usecase.PersistentObject;

import java.util.List;
import java.util.Optional;

public interface RepositoryImpl<T extends PersistentObject> {
    void save(T po);
    void delete(String id);
    Optional<T> findById(String id);
    List<T> findAll();
}
