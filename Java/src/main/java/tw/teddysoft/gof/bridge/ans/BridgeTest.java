/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.bridge.ans;

import org.junit.jupiter.api.Test;
import tw.teddysoft.gof.bridge.ans.adapter.BoardRepository;
import tw.teddysoft.gof.bridge.ans.adapter.CardRepository;
import tw.teddysoft.gof.bridge.ans.adapter.FakeMySQLRepositoryImpl;
import tw.teddysoft.gof.bridge.ans.adapter.InMemoryRepositoryImpl;
import tw.teddysoft.gof.bridge.ans.entity.Board;
import tw.teddysoft.gof.bridge.ans.entity.Card;
import tw.teddysoft.gof.bridge.ans.usecase.Repository;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BridgeTest {

	@Test
	public void testInMemoryBoardRepository(){

		Repository<Board> boardRepository = new BoardRepository(new InMemoryRepositoryImpl());
		String boardId = UUID.randomUUID().toString();
		Board board = new Board(boardId, "Scrum");

		boardRepository.save(board);

		Board storedBoard = boardRepository.findById(boardId).get();
		assertEquals(boardId, storedBoard.getId());
		assertEquals("Scrum", storedBoard.getBoardName());
	}

	@Test
	public void testFakeMySqlBoardRepository(){

		Repository<Board> boardRepository = new BoardRepository(new FakeMySQLRepositoryImpl());
		String boardId = UUID.randomUUID().toString();
		Board board = new Board(boardId, "Scrum");

		boardRepository.save(board);

		Board storedBoard = boardRepository.findById(boardId).get();
		assertEquals(boardId, storedBoard.getId());
		assertEquals("Scrum", storedBoard.getBoardName());
	}

	@Test
	public void testInMemoryCardRepository(){

		Repository<Card> cardRepository = new CardRepository(new InMemoryRepositoryImpl());
		String cardId = UUID.randomUUID().toString();
		Card card = new Card(cardId, "Implement bridge pattern", "Teddy");

		cardRepository.save(card);

		Card storedCard = cardRepository.findById(cardId).get();
		assertEquals(cardId, storedCard.getId());
		assertEquals("Implement bridge pattern", storedCard.getCardName());
		assertEquals("Teddy", storedCard.getAssignee());
	}

	@Test
	public void testFakeMySqlCardRepository(){

		Repository<Card> cardRepository = new CardRepository(new FakeMySQLRepositoryImpl());
		String cardId = UUID.randomUUID().toString();
		Card card = new Card(cardId, "Implement bridge pattern", "Teddy");

		cardRepository.save(card);

		Card storedCard = cardRepository.findById(cardId).get();
		assertEquals(cardId, storedCard.getId());
		assertEquals("Implement bridge pattern", storedCard.getCardName());
		assertEquals("Teddy", storedCard.getAssignee());
	}

}
