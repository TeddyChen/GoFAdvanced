package tw.teddysoft.gof.bridge.exercise.usecase;

import java.util.List;

public interface PersistentObject {
    String getId();

    List<DomainEventPo> getDomainEvents();
}
