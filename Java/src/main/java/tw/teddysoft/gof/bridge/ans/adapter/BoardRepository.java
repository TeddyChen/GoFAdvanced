package tw.teddysoft.gof.bridge.ans.adapter;

import tw.teddysoft.gof.bridge.ans.usecase.Repository;
import tw.teddysoft.gof.bridge.ans.usecase.RepositoryImpl;
import tw.teddysoft.gof.bridge.ans.entity.Board;
import tw.teddysoft.gof.bridge.ans.usecase.BoardPo;

import java.util.ArrayList;
import java.util.Optional;

public class BoardRepository extends Repository<Board> {
    public BoardRepository(RepositoryImpl implementation) {
        super(implementation);
    }
    @Override
    public void save(Board board) {
        BoardPo po = new BoardPo(board.getId(), board.getBoardName(), new ArrayList<>());
        impl.save(po);
    }

    @Override
    public Optional<Board> findById(String id) {
        Optional<BoardPo> boardPo = impl.findById(id);
        return boardPo.map(po -> new Board(po.getId(), po.getBoardName()));
    }
}
