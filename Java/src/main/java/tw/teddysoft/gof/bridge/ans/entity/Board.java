package tw.teddysoft.gof.bridge.ans.entity;

public class Board extends AggregateRoot {
    private String boardName;

    public Board(String boardId, String boardName) {
        super(boardId);
        this.boardName = boardName;
    }
    public String getBoardName() {
        return boardName;
    }
    public void setBoardName(String boardName) {
        this.boardName = boardName;
    }
}
