package tw.teddysoft.gof.bridge.ans.adapter;

import tw.teddysoft.gof.bridge.ans.usecase.PersistentObject;
import tw.teddysoft.gof.bridge.ans.usecase.RepositoryImpl;

import java.util.*;

public class FakeMySQLRepositoryImpl<T extends PersistentObject> implements RepositoryImpl<T> {

    private List<T> stores = new ArrayList<>();
    @Override
    public void save(T po) {
        stores.add(po);
    }

    @Override
    public void delete(String id) {
        stores.removeIf( x -> x.getId().equals(id));
    }

    @Override
    public Optional<T> findById(String id) {

        return stores.stream().filter( x -> x.getId().equals(id)).findFirst();
    }

    @Override
    public List<T> findAll() {
        return Collections.unmodifiableList(stores);
    }
}
