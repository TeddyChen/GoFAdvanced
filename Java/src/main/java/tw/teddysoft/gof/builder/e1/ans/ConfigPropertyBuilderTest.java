/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.builder.e1.ans;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class ConfigPropertyBuilderTest {

	@Test
	public void testHowToUsePlainTextConfigPropertyBuilder() throws ConfigurationError {

		ConfigPropertyBuilder builder = new PlainTextConfigPropertyBuilder();
		builder.timeout(30);
		builder.platform("Ubuntu");
		builder.location("\\opt\\property.txt");

		assertEquals("TIMEOUT=30\nPLATFORM=Ubuntu\nLOCATION=\\opt\\property.txt\n", builder.build());
	}
	
	@Test
	public void testHowToUseJsonConfigPropertyBuilder() throws ConfigurationError {

		ConfigPropertyBuilder builder = new JsonConfigPropertyBuilder();
		builder.timeout(30);
		builder.platform("Ubuntu");
		builder.location("\\opt\\property.txt");
		
		assertEquals("{\"PLATFORM\":\"Ubuntu\",\"TIMEOUT\":30,\"LOCATION\":\"\\opt\\property.txt\"}",
				builder.build());
	}

	
	@Test
	public void testPlainTextConfigPropertyBuilderLocationNotBeSetException()  {

	    try{
	    	ConfigPropertyBuilder builder = new PlainTextConfigPropertyBuilder();
	    	builder.timeout(30);
	    	builder.platform("Ubuntu");
	    	builder.build();
	    	
	    	fail("builder.build() should have thrown an exception");
	    }
	    catch(ConfigurationError e){
	    	assertEquals("The LOCATION property must be set.", e.getMessage());
	    }
	}
	
	
	@Test
	public void testJsonConfigPropertyBuilderLocationNotBeSetException()  {

	    try{
	    	ConfigPropertyBuilder builder = new JsonConfigPropertyBuilder();
	    	builder.timeout(30);
	    	builder.platform("Ubuntu");
	    	builder.build();
	    	
	    	fail("builder.build() should have thrown an exception");
	    }
	    catch(ConfigurationError e){
	    	assertEquals("The LOCATION property must be set.", e.getMessage());
	    }
	}

}
