///*
// * Copyright 2016 TeddySoft Technology. All rights reserved.
// *
// */
//package tw.teddysoft.gof.builder.e2.exercise;
//
//import org.junit.jupiter.api.Test;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//
//public class ConfigPropertyBuilderTest {
//
//	@Test
//	public void testHowToUsePlainTextConfigPropertyBuilder() throws ConfigurationError {
//
//		String configString = PlainTextConfigPropertyBuilder.newBuilder()
//										.timeout(30)
//										.platform("Ubuntu")
//										.location("\\opt\\property.txt")
//										.build();
//
//		assertEquals("TIMEOUT=30\nPLATFORM=Ubuntu\nLOCATION=\\opt\\property.txt\n", configString);
//	}
//
//	@Test
//	public void testPlainTextConfigPropertyBuilderLocationNotBeSetException()  {
//
//	    try{
//			String configString = PlainTextConfigPropertyBuilder.newBuilder()
//									.timeout(30)
//									.platform("Ubuntu")
//									.build();
//
//	    	fail("builder.build() should have thrown an exception");
//	    }
//	    catch(ConfigurationError e){
//	    	assertEquals("The LOCATION property must be set.", e.getMessage());
//	    }
//	}
//
//
//	@Test
//	public void testHowToUseJsonConfigPropertyBuilder() throws ConfigurationError {
//
//		String configString = JsonConfigPropertyBuilder.newBuilder()
//				.timeout(30)
//				.platform("Ubuntu")
//				.location("\\opt\\property.txt")
//				.build();
//
//		assertEquals("{\"PLATFORM\":\"Ubuntu\",\"TIMEOUT\":30,\"LOCATION\":\"\\opt\\property.txt\"}",
//				configString);
//	}
//
//
//	@Test
//	public void testJsonConfigPropertyBuilderLocationNotBeSetException()  {
//
//	    try{
//			String configString = JsonConfigPropertyBuilder.newBuilder()
//					.timeout(30)
//					.platform("Ubuntu")
//					.build();
//
//	    	fail("builder.build() should have thrown an exception");
//	    }
//	    catch(ConfigurationError e){
//	    	assertEquals("The LOCATION property must be set.", e.getMessage());
//	    }
//	}
//}
