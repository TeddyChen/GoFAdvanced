/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.builder.e2.ans;

import java.util.HashMap;
import java.util.LinkedHashMap;

public class PlainTextConfigPropertyBuilder implements ConfigPropertyBuilder {
	private HashMap<String, String> _table;
		
	private PlainTextConfigPropertyBuilder(){
		_table = new LinkedHashMap<>();
	}
	
	public static ConfigPropertyBuilder newBuilder(){
		return new PlainTextConfigPropertyBuilder();
	}
	
	@Override
	public ConfigPropertyBuilder platform(String aValue){
		_table.put("PLATFORM", aValue);
		return this;
	}
	
	@Override
	public ConfigPropertyBuilder timeout(int aValue){
		_table.put("TIMEOUT", String.valueOf(aValue));
		return this;
	}
	
	@Override
	public ConfigPropertyBuilder location(String aPath){
		_table.put("LOCATION", aPath);
		return this;
	}
	
	@Override
	public String build() throws ConfigurationError{
		if (!_table.containsKey("LOCATION") ){
			throw new ConfigurationError("The LOCATION property must be set.");
		}
		
		StringBuffer sb = new StringBuffer();
		for(String s : _table.keySet()){
			sb.append(s).append("=").append(_table.get(s)).append("\n");
		}
		return sb.toString();
	}
}


