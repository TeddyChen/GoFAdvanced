/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.builder.e2.ans;

public class JsonConfigPropertyBuilder implements ConfigPropertyBuilder {
	String platform = null;
	int timeout = -1;
	String location = null;
	
	private JsonConfigPropertyBuilder(){
	}
	
	public static ConfigPropertyBuilder newBuilder(){
		return new JsonConfigPropertyBuilder();
	}
	
	@Override
	public ConfigPropertyBuilder platform(String aValue){
		platform = aValue;
		return this;
	}
	
	@Override
	public ConfigPropertyBuilder timeout(int aValue){
		timeout = aValue;
		return this;
	}
	
	@Override
	public ConfigPropertyBuilder location(String aPath){
		location = aPath;
		return this;
	}
	
	@Override
	public String build() throws ConfigurationError{
		if (null == location){
			throw new ConfigurationError("The LOCATION property must be set.");
		}
		
		StringBuffer sb = new StringBuffer();
		sb.append("{");
		if(null != platform) {
			sb.append(getJsonElementString("PLATFORM", platform)).append(",");
		}
		sb.append(getJsonElementString("TIMEOUT", timeout));
		sb.append(",").append(getJsonElementString("LOCATION", location));
		sb.append("}");
		return sb.toString();
	}
	
	private String getJsonElementString(String aKey, String aValue){
		return "\"" + aKey + "\":" + "\"" + aValue + "\"";
	}
	
	private String getJsonElementString(String aKey, int aValue){
		return "\"" + aKey + "\":"  + aValue;
	}
}
