/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.builder.e2.ans;

public interface ConfigPropertyBuilder {

	ConfigPropertyBuilder platform(String aValue);
	ConfigPropertyBuilder timeout(int aValue);
	ConfigPropertyBuilder location(String aPath);

	String build() throws ConfigurationError;
}