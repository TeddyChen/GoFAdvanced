/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.builder.e1.exercise;

import java.util.HashMap;
import java.util.LinkedHashMap;

public class ConfigProperty {
	private HashMap<String, String> _table = new LinkedHashMap<>();
		
	public void put(String aKey, String aValue){
		_table.put(aKey, aValue);
	}
	
	public String get(String aKey){
		return _table.get(aKey);
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();

		for(String s : _table.keySet()){
			sb.append(s).append("=").append(_table.get(s)).append("\n");
		}
		return sb.toString();
	}
}
