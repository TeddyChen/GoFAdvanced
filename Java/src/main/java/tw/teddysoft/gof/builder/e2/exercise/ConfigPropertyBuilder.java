/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.builder.e2.exercise;

public interface ConfigPropertyBuilder {
	void platform(String aValue);
	void timeout(int aValue);
	void location(String aPath);
	String build() throws ConfigurationError;
}