package tw.teddysoft.gof.decorator.ans.entity;

import java.util.ArrayList;
import java.util.List;

public abstract class AggregateRoot {
    protected String id;
    protected List<DomainEvent> domainEvents;
    protected long version;

    public AggregateRoot(String id){
        this();
        this.id = id;
    }

    public AggregateRoot(List<? extends DomainEvent> events) {
        this();
        this.version = 0;
        events.forEach(x-> apply(x));
        clearDomainEvents();
    }

    protected AggregateRoot() {
        super();
        this.domainEvents = new ArrayList<>();
        this.version = -1;
    }

    public String getId(){
        return id;
    }
    public void addDomainEvent(DomainEvent event){
        domainEvents.add(event);
    }
    public List<DomainEvent> getDomainEvents(){
        return domainEvents;
    }
    public void clearDomainEvents(){
        domainEvents.clear();
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public final void apply(DomainEvent event) {
        when(event);
        addDomainEvent(event);
    }

    protected abstract void when(DomainEvent event);
}
