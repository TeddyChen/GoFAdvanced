package tw.teddysoft.gof.decorator.exercise.entity;

import java.util.*;

public class Card extends AggregateRoot implements
        AggregateSnapshot<Card.CardSnapshot> {

    @Override
    public CardSnapshot getSnapshot() {
        return new CardSnapshot(laneId, id, description, assignees, deadline, version);
    }

    @Override
    public void setSnapshot(CardSnapshot snapshot) {
        this.laneId = snapshot.laneId;
        this.id = snapshot.cardId;
        this.description = snapshot.description;
        this.assignees = snapshot.assignees;
        this.deadline = snapshot.deadline;
        this.version = snapshot.version;
    }

    private String laneId;
    private String description;
    private List<String> assignees;
    private Date deadline;

    public record CardSnapshot(
            String laneId,
            String cardId,
            String description,
            List<String> assignees,
            Date deadline, long version){}

    public static Card fromSnapshot(CardSnapshot snapshot){
        Card card = new Card();
        card.setSnapshot(snapshot);
        return card;
    }



    protected Card(){};

    public Card(List<DomainEvent> domainEvents){
        super(domainEvents);
    }

    public Card(String laneId, String cardId, String description) {
        apply(new CardEvents.CardCreated(
                laneId,
                cardId,
                description,
                deadline,
                UUID.randomUUID(),
                new Date()));
    }

    public void changeDescription(String newDescription) {
        apply(new CardEvents.CardDescriptionChanged(
                getId(),
                newDescription,
                UUID.randomUUID(),
                new Date()));
    }


    public void changeDeadline(Date newDeadline) {
        apply(new CardEvents.CardDeadlineChanged(
                this.getId(),
                newDeadline,
                UUID.randomUUID(),
                new Date()));
    }

    public String getDescription() {
        return description;
    }

    public List<String> getAssignees() {
        return Collections.unmodifiableList(assignees);
    }

    public void assign(String assignee) {
        apply(new CardEvents.CardAssigned(
                this.id,
                assignee,
                UUID.randomUUID(),
                new Date()));
    }

    public void unassign(String unassignee) {
        apply(new CardEvents.CardUnassigned(
                this.id,
                unassignee,
                UUID.randomUUID(),
                new Date()));
    }

    public Date getDeadline() {
        return deadline;
    }

    @Override
    protected void when(DomainEvent event) {
        switch (event){
            case CardEvents.CardCreated e -> {
                laneId = e.laneId();
                id = e.cardId();
                description = e.description();
                deadline = e.deadline();
                assignees = new ArrayList<>();
            }
            case CardEvents.CardDescriptionChanged e -> this.description = e.description();
            case CardEvents.CardAssigned e -> assignees.add(e.assignee());
            case CardEvents.CardUnassigned e -> assignees.removeIf(x -> x.equals(e.unassignee()));
            case CardEvents.CardDeadlineChanged e -> this.deadline = e.deadline();
            default -> {}
        }
    }
}
