package tw.teddysoft.gof.decorator.ans.adapter;

import tw.teddysoft.gof.decorator.ans.entity.Card;
import tw.teddysoft.gof.decorator.ans.entity.DomainEvent;
import tw.teddysoft.gof.decorator.ans.entity.Json;
import tw.teddysoft.gof.decorator.ans.usecase.EsRepository;

import java.util.*;

public class CardMemoryCacheRepositoryDecorator extends RepositoryDecorator<Card>  {
    private Map<String, Card> cache;
    public CardMemoryCacheRepositoryDecorator(EsRepository component) {
        super(component);
        cache = new HashMap<>();
    }

    @Override
    public void save(Card card) {
        component.save(card);
        cache.put(card.getId(), card);
    }

    @Override
    public Optional<Card> findById(String cardId) {
        if (cache.containsKey(cardId)) {
            System.out.println("Cache hit!");
            return Optional.of(cache.get(cardId));
        }

        Optional<Card> card = component.findById(cardId);
        if (card.isPresent()) cache.put(cardId, card.get());
        return card;
    }

    @Override
    public void delete(Card card) {
        component.delete(card);
        cache.remove(card.getId());
    }
}
