package tw.teddysoft.gof.decorator.ans.adapter;

import tw.teddysoft.gof.decorator.ans.entity.Card;
import tw.teddysoft.gof.decorator.ans.entity.DomainEvent;
import tw.teddysoft.gof.decorator.ans.usecase.EsRepository;

import java.util.*;
import java.util.stream.Collectors;

public class CardEsRepository implements EsRepository<Card> {
    private Map<String, List<DomainEvent>> stores = new HashMap<>();

    @Override
    public void save(Card entity) {
        if (!stores.containsKey(entity.getId())){
            stores.put(entity.getId(), new ArrayList<>());
        }

        long version = stores.get(entity.getId()).size() + entity.getDomainEvents().size();
        stores.get(entity.getId()).addAll(entity.getDomainEvents());
        entity.setVersion(version);
        entity.clearDomainEvents();
    }

    @Override
    public Optional<Card> findById(String id) {
        List<DomainEvent> events = stores.get(id);
        if (events.isEmpty()) return Optional.empty();

        Card card = new Card(events);
        return Optional.of(card);
    }

    @Override
    public void delete(Card entity) {
        this.save(entity);
    }

    @Override
    public Optional<DomainEvent> getLastEventFromStream(String streamName){
        List<DomainEvent> events = stores.get(streamName);
        if (null == events) return Optional.empty();

        return Optional.of(events.get(events.size() - 1));
    }


    @Override
    public void saveEvent(String streamName, DomainEvent event) {
        if (!stores.containsKey(streamName)){
            stores.put(streamName, new ArrayList<>());
        }
        stores.get(streamName).add(event);
    }

    @Override
    public List<DomainEvent> getEventsFromStream(String streamName, long version) {
        List<DomainEvent> events = stores.get(streamName);
        if (events.isEmpty()) return events;

        return events.stream()
                .skip(version)
                .collect(Collectors.toList());
    }
}
