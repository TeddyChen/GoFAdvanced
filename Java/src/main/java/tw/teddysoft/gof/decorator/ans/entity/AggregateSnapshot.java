package tw.teddysoft.gof.decorator.ans.entity;

public interface AggregateSnapshot<T> {
    T getSnapshot();
    void setSnapshot(T snapshot);
}
