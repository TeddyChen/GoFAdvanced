package tw.teddysoft.gof.decorator.exercise.usecase;


import tw.teddysoft.gof.decorator.exercise.entity.DomainEvent;
import tw.teddysoft.gof.decorator.exercise.entity.AggregateRoot;

import java.util.List;
import java.util.Optional;

public interface EsRepository<T extends AggregateRoot> {
    Optional<T> findById(String id);
    void save(T entity);
    void delete(T entity);
    Optional<DomainEvent> getLastEventFromStream(String streamName);
    void saveEvent(String streamName, DomainEvent event);
    List<DomainEvent> getEventFromStream(String streamName, long version);
}
