///*
// * Copyright 2016 TeddySoft Technology. All rights reserved.
// *
// */
//package tw.teddysoft.gof.iterator.exercise;
//
//import org.junit.jupiter.api.Test;
//import tw.teddysoft.gof.iterator.ans.Iterator;
//
//public class BookListTest {
//
//	@Test
//	public void testIterator() {
//
//		BookList bookList = new BookList();
//		bookList.addBook(new Book("笑談軟體工程", "Teddy", "悅之"));
//		bookList.addBook(new Book("The Timeless Way of Building", "Christopher Alexander", "Oxford University Press"));
//		bookList.addBook(new Book("Design Patterns", "GoF", "Addison-Wesley"));
//		bookList.addBook(new Book("Object-Oriented Software Construction, 2nd", "Bertrand Meyer", "Prentice Hall"));
//		bookList.addBook(new Book("Clean Code", "Robert C. Martin", "Prentice Hall"));
//
//		for (Iterator iterator = bookList.createIterator(); iterator.hasNext(); ) {
//			   System.out.println(iterator.next().toString());
//		}
//
//		Iterator iterator = bookList.createIterator();
//	    while (iterator.hasNext() ){
//		      System.out.println(iterator.next().toString());
//		}
//	}
//}
