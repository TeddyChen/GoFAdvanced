/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.iterator.exercise;

public class Book {
	private String title;
	private String author;
	private String publisher;

	public Book(String aTitle, String aAuthor, 
			String aPublisher){
		title = aTitle;
		author = aAuthor;
		publisher = aPublisher;
	}
	public String getTitle(){
		return title;
	}
	public String getAuthor(){
		return author;
	}
	public String getPublisher(){
		return publisher;
	}
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Title = ").append(title).append("; ");
		sb.append("Author = ").append(author).append("; ");
		sb.append("Publisher = ").append(publisher).append(".");
		return sb.toString();
	}
}
