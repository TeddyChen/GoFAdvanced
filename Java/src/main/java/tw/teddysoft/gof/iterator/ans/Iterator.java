/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.iterator.ans;

public interface Iterator {
	Object next();
	boolean hasNext();
}
