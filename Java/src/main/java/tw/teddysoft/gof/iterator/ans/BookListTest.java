/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.iterator.ans;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BookListTest {

	@Test
	public void testIterator() {

		BookList bookList = new BookList();
		bookList.addBook(new Book("笑談軟體工程", "Teddy", "悅之"));
		bookList.addBook(new Book("The Timeless Way of Building", "Christopher Alexander", "Oxford University Press"));
		bookList.addBook(new Book("Design Patterns", "GoF", "Addison-Wesley"));
		bookList.addBook(new Book("Object-Oriented Software Construction, 2nd", "Bertrand Meyer", "Prentice Hall"));
		bookList.addBook(new Book("Clean Code", "Robert C. Martin", "Prentice Hall"));
		
		String expected = "Title = 笑談軟體工程; Author = Teddy; Publisher = 悅之.Title = The Timeless Way of Building; Author = Christopher Alexander; Publisher = Oxford University Press.Title = Design Patterns; Author = GoF; Publisher = Addison-Wesley.Title = Object-Oriented Software Construction, 2nd; Author = Bertrand Meyer; Publisher = Prentice Hall.Title = Clean Code; Author = Robert C. Martin; Publisher = Prentice Hall.";

		StringBuilder sb = new StringBuilder();
		for (Iterator iterator = bookList.createIterator(); iterator.hasNext(); ) {
			  String item = iterator.next().toString();
			   System.out.println(item);
			   sb.append(item);
		}
		assertEquals(expected, sb.toString());

		sb = new StringBuilder();
		Iterator iterator = bookList.createIterator();
	    while (iterator.hasNext() ){
			  String item = iterator.next().toString();
			   System.out.println(item);
			   sb.append(item);
		}
	    assertEquals(expected, sb.toString());
	}
}
