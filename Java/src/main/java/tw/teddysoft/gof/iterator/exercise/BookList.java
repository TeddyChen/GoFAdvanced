/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.iterator.exercise;

import java.util.LinkedList;
import java.util.List;

public class BookList {
	private List<Book> list;
	
	public BookList(){
		list = new LinkedList<>();
	}
	
	public void addBook(Book aBook){
		list.add(aBook);
	}
	
	public int size(){
		return list.size();
	}
	
	public boolean isEmpty(){
		return list.isEmpty();
	}
	
	public Book getBook(int aIndex){
		return list.get(aIndex);
	}
}
