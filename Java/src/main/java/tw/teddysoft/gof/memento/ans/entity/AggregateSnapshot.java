package tw.teddysoft.gof.memento.ans.entity;

public interface AggregateSnapshot<T> {
    T getSnapshot();
    void setSnapshot(T snapshot);
}
