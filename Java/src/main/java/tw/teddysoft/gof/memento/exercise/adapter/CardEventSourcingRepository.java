package tw.teddysoft.gof.memento.exercise.adapter;

import tw.teddysoft.gof.memento.exercise.entity.Card;
import tw.teddysoft.gof.memento.exercise.usecase.Repository;

import java.util.Optional;

public class CardEventSourcingRepository implements Repository<Card>  {
    private final CardStore cardStore;

    public CardEventSourcingRepository(CardStore cardStore) {
        this.cardStore = cardStore;
    }

    @Override
    public void save(Card card) {
        cardStore.save(card);
        card.clearDomainEvents();
    }

    @Override
    public Optional<Card> findById(String cardId) {
        return cardStore.findById(cardId);
    }

    @Override
    public void delete(Card card) {
        cardStore.delete(card);
    }
}
