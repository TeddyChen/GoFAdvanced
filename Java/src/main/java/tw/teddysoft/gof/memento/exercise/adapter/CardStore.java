package tw.teddysoft.gof.memento.exercise.adapter;

import tw.teddysoft.gof.memento.exercise.entity.Card;
import tw.teddysoft.gof.memento.exercise.entity.DomainEvent;

import java.util.*;
import java.util.stream.Collectors;

public class CardStore {

    private Map<String, List<DomainEvent>> stores = new HashMap<>();

    public void save(Card entity) {
        if (!stores.containsKey(entity.getId())){
            stores.put(entity.getId(), new ArrayList<>());
        }

        long version = stores.get(entity.getId()).size() + entity.getDomainEvents().size();
        stores.get(entity.getId()).addAll(entity.getDomainEvents());
        entity.setVersion(version);
    }

    public void delete(Card entity) {
        this.save(entity);
    }

    public Optional<Card> findById(String id) {
        List<DomainEvent> events = stores.get(id);
        if (events.isEmpty()) return Optional.empty();

        Card card = new Card(events);
        return Optional.of(card);
    }

    public Optional<DomainEvent> getLastEventFromStream(String streamName){
        List<DomainEvent> events = stores.get(streamName);
        if (null == events) return Optional.empty();

        return Optional.of(events.get(events.size() - 1));
    }


    public void saveEvent(String streamName, DomainEvent event) {
        if (!stores.containsKey(streamName)){
            stores.put(streamName, new ArrayList<>());
        }
        stores.get(streamName).add(event);
    }

    public List<DomainEvent> getEventFromStream(String streamName, long version) {
        List<DomainEvent> events = stores.get(streamName);
        if (events.isEmpty()) return events;

        return events.stream()
                .skip(version)
                .collect(Collectors.toList());
    }
}
