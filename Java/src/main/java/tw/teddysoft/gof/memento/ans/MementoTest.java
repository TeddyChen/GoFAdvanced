/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.memento.ans;

import org.junit.jupiter.api.Test;
import tw.teddysoft.gof.memento.ans.adapter.CardStore;
import tw.teddysoft.gof.memento.ans.adapter.CardEventSourcingRepository;
import tw.teddysoft.gof.memento.ans.entity.Card;
import tw.teddysoft.gof.memento.ans.entity.DomainEvent;
import tw.teddysoft.gof.memento.ans.usecase.Repository;

import java.util.Date;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MementoTest {
	@Test
	public void generate_card_snapshot_and_read_card_from_it() {
		CardStore cardStore = new CardStore();
		Repository<Card> cardRepository = new CardEventSourcingRepository(cardStore);

		Card card = new Card("lane-001", UUID.randomUUID().toString(), "my card");
		card.assign("Teddy");
		card.assign("Eiffel");
		card.assign("Ada");
		card.assign("Pascal");
		card.changeDeadline(new Date());
		cardRepository.save(card);
		card.changeDescription("new card description");
		card.unassign("Teddy");
		cardRepository.save(card);

		Card storedCard = cardRepository.findById(card.getId()).get();
		assertEquals(card.getDescription(), storedCard.getDescription());
		assertEquals(card.getAssignees().size(), storedCard.getAssignees().size());

		DomainEvent.Snapshotted snapshotted = (DomainEvent.Snapshotted) cardStore.getLastEventFromStream(
				CardEventSourcingRepository.getSnapshottedStreamName(card.getId())).get();
		System.out.println(snapshotted.snapshot());
	}
}
