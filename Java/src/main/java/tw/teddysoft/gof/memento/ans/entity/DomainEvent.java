package tw.teddysoft.gof.memento.ans.entity;

import java.io.Serializable;
import java.time.Instant;
import java.util.Date;
import java.util.UUID;

public interface DomainEvent extends Serializable {
    UUID id();
    Date occurredOn();
    String aggregateId();

    record Snapshotted(
            String aggregateId,
            String snapshot,
            long version,
            UUID id,
            Date occurredOn
    ) implements DomainEvent {}
}
