package tw.teddysoft.gof.memento.exercise.entity;

public interface AggregateSnapshot<T> {
    T getSnapshot();
    void setSnapshot(T snapshot);
}
