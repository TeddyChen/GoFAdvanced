/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.cor.ans;

public class StateChangeHandler extends ServiceHandler {
	
	@Override
	protected void internalHandler(Service service, StringBuilder result) {
		if (service.isStateChanged()) {
			result.append("State change from " + service.getPreviousState() +
					" to " + service.getCurrentState() + ".\n");
		}
	}
}
