/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.cor.ans;

public abstract class ServiceHandler {
	private ServiceHandler next = null;
	
	public final void handle(Service service, StringBuilder result) {
		internalHandler(service, result);
		
		if(null != next)
			next.handle(service, result);
	}

	public final void setNext(ServiceHandler handler) {
		next = handler;
	}
	
	protected abstract void internalHandler(Service service, 
			StringBuilder result);
}
