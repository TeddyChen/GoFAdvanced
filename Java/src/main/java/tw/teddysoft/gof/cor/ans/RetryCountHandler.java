/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.cor.ans;

public class RetryCountHandler extends ServiceHandler {
	
	@Override
	protected void internalHandler(Service service, StringBuilder result) {
		if (service.getRetry() >=5) {
			// auto adjust the check interval
			result.append("Too many attempts please try again later.\n");
		}
	}
}
