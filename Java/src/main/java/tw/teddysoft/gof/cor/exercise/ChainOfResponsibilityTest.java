///*
// * Copyright 2016 TeddySoft Technology. All rights reserved.
// *
// */
//package tw.teddysoft.gof.cor.exercise;
//
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import tw.teddysoft.gof.cor.ans.CriticalDurationHandler;
//import tw.teddysoft.gof.cor.ans.RetryCountHandler;
//import tw.teddysoft.gof.cor.ans.ServiceHandler;
//import tw.teddysoft.gof.cor.ans.StateChangeHandler;
//
//import java.text.DateFormat;
//import java.text.ParseException;
//import java.util.Date;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//
//public class ChainOfResponsibilityTest {
//
//	private Service mySqlCritical;
//	private Service userDefinedCritical;
//	private Service userDefinedOk;
//	private StringBuilder result;
//
//	@BeforeEach
//	public void setup() throws ParseException{
//		mySqlCritical = new Service(ServiceType.Database, "MySQL 6.5");
//		userDefinedCritical = new Service(ServiceType.UserDefined, "My Microservice");
//		userDefinedOk = new Service(ServiceType.UserDefined, "My Microservice");
//		result = new StringBuilder();
//
//		for(int i = 0; i < 10; i++){
//			mySqlCritical.incRetry();
//			userDefinedCritical.incRetry();
//		}
//
//		DateFormat df = DateFormat.getDateInstance();
//		Date date = df.parse("2017/7/19");
//		mySqlCritical.setCurrentState(State.Critical, date);
//		userDefinedCritical.setCurrentState(State.Critical, date);
//		userDefinedOk.setCurrentState(State.Ok, date);
//	}
//
//
//	@Test
//	public void when_too_many_attepmts_and_critical_state_over_one_day_for_database_service() {
//		String expectedResult = "Too many attempts please try again later.\n" +
//				"Escalation due to critical state over one day.\n";
//
//		ServiceHandler retryCountHandler = new RetryCountHandler();
//		ServiceHandler criticalDurationHandler = new CriticalDurationHandler();
//		retryCountHandler.setNext(criticalDurationHandler);
//
//		retryCountHandler.handle(mySqlCritical, result);
//		assertEquals(expectedResult, result.toString());
//	}
//
//	@Test
//	public void when_too_many_attepmts_and_critical_state_over_one_day_for_userdefined_serivce() {
//		String expectedResult = "Too many attempts please try again later.\n" +
//				"Escalation due to critical state over one day.\n" +
//				"State change from Pending to Critical.\n";
//
//		ServiceHandler retryCountHandler = new RetryCountHandler();
//		ServiceHandler criticalDurationHandler = new CriticalDurationHandler();
//		ServiceHandler stateChangeHandler = new StateChangeHandler();
//
//		retryCountHandler.setNext(criticalDurationHandler);
//		criticalDurationHandler.setNext(stateChangeHandler);
//
//		retryCountHandler.handle(userDefinedCritical, result);
//		assertEquals(expectedResult, result.toString());
//	}
//
//
//	@Test
//	public void when_too_many_attepmts_and_Ok_state_over_one_day_for_userdefined_serivce_status() {
//		String expectedResult = "State change from Pending to Ok.\n";
//
//		ServiceHandler retryCountHandler = new RetryCountHandler();
//		ServiceHandler criticalDurationHandler = new CriticalDurationHandler();
//		ServiceHandler stateChangeHandler = new StateChangeHandler();
//
//		retryCountHandler.setNext(criticalDurationHandler);
//		criticalDurationHandler.setNext(stateChangeHandler);
//
//		retryCountHandler.handle(userDefinedOk, result);
//		assertEquals(expectedResult, result.toString());
//	}
//
//}
//
