/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.cor.ans;

public enum ServiceType {
	Database,
	Http,
	Email,
	Ftp,
	UserDefined;
}
