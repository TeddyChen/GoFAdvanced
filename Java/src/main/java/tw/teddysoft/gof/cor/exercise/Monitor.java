/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.cor.exercise;

import java.util.Date;

public class Monitor {
	
	public String handleServiceCheck(Service service){
		StringBuilder result = new StringBuilder();
		
		switch (service.getType()) {
		case Database:
			checkRetry(service, result);
			checkCriticalDuration(service, result);
			break;
		case UserDefined:
			checkRetry(service, result);
			checkCriticalDuration(service, result);
			checkStateChange(service, result);
			break;
		default:
			break;
		}
		
		return result.toString();
	}

	private void checkRetry(Service service, StringBuilder result){
		if (service.getRetry() >=5) {
			// auto adjust the check interval
			result.append("Too many attempts please try again later.\n");
		}
	}
	
	private void checkCriticalDuration(Service service, StringBuilder result){
		if(State.Critical == service.getCurrentState() && 
				afterOneDay(service.getLastStateChange())){
			// send an email to the manager
			result.append("Escalation due to critical state over one day.\n");
		}
	}
	
	private void checkStateChange(Service service, StringBuilder result){
		if (service.isStateChanged()) {
			result.append("State change from " + service.getPreviousState() + 
					" to " + service.getCurrentState() + ".\n");
		}
	}
	
	private boolean afterOneDay(Date date){
		return (date.before(new Date())) ? true : false;
	}
	
	
}
