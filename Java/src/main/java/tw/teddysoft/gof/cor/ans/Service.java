/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.cor.ans;

import java.util.Date;

public class Service {
	private String name;
	private ServiceType type;
	private int retry = 0;
	private State currentState;
	private State previousState;
	private Date stateChange;
	private int checkCount = 0;
	
	public Service(ServiceType type, String name){
		this.type = type;
		this.name = name;
		currentState = State.Pending;
		previousState = State.Pending;
	}
	
	public ServiceType getType(){
		return type;
	}
	
	public String getName(){
		return name;
	}
	
	public State getCurrentState(){
		return currentState;
	}
	
	public void setCurrentState(State newState, Date date){
		stateChange = date;
		previousState = currentState;
		currentState = newState;
	}
	
	public State getPreviousState(){
		return previousState;
	}
	
	public void incRetry(){
		retry++;
	}
	public void resetRetry(){
		retry = 0;
	}
	
	public int getRetry(){
		return retry;
	}

	public Date getLastStateChange(){
		return stateChange;
	}
	
	public long getCheckedCount(){
		return checkCount;
	}
	
	public void incCheckedCount(){
		checkCount++;
	}
	
	public boolean isStateChanged(){
		return currentState == previousState ? false : true;
	}
	
}
