///*
// * Copyright 2016 TeddySoft Technology. All rights reserved.
// *
// */
//package tw.teddysoft.gof.proxy.exercise;
//
//import org.junit.jupiter.api.Test;
//import tw.teddysoft.gof.proxy.ans.UIDescriptionManagerProxy;
//
//import java.util.Date;
//
//import static org.junit.jupiter.api.Assertions.assertTrue;
//
//public class UIDescriptionManagerTest {
//
//	@Test
//	public void testUseRealObjectToLoadUIDescription() throws DescriptionNotFoundException {
//
//		UIDescriptionManager manager = new UIDescriptionManagerImpl();
//		Date start = new Date();
//		manager.getDescription("ID_LIST_PRODUCT");
//		manager.getDescription("ID_LIST_PRODUCT");
//		manager.getDescription("ID_LIST_PRODUCT");
//		Date end = new Date();
//
//		assertTrue(6000 <= end.getTime()-start.getTime());
//	}
//
//	@Test
//	public void testUseProxyToLoadUIDescription() throws DescriptionNotFoundException {
//
//		UIDescriptionManager manager = new UIDescriptionManagerProxy(new UIDescriptionManagerImpl());
//		Date start = new Date();
//		manager.getDescription("ID_LIST_PRODUCT");
//		manager.getDescription("ID_LIST_PRODUCT");
//		manager.getDescription("ID_LIST_PRODUCT");
//		Date end = new Date();
//
//		assertTrue(3000 > end.getTime()-start.getTime());
//	}
//
//
//}
