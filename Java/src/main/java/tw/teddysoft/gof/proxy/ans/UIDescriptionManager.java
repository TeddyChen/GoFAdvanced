/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.proxy.ans;

public interface UIDescriptionManager {
	String getDescription(String aID) throws DescriptionNotFoundException;
}
