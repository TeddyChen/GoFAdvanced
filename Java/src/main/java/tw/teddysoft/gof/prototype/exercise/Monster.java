/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.prototype.exercise;

public class Monster {

	private int hp = 100;	
	private int power = 50;
	private Exterior ext = null;
	
	public Monster (int hp, int power, String aFileName){
		this.hp = hp;
		this.power = power;
		this.ext =  new Exterior(aFileName);
	}
	
	public int getHP(){
		return hp;
	}
	
	public int getPower(){
		return power;
	}
	
	public Exterior getExterior(){
		return ext;
	}
	
	public void display(){
		System.out.println("HP: " + hp + ", Power: " + power + "\n");
	}
	
	public void hit(){
		this.hp -= 10;
		this.power += 5;
	}
}
