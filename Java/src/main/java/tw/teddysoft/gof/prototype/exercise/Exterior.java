/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.prototype.exercise;

public class Exterior {
	public String source = null;
	
	public Exterior(String aFileName){
		source = aFileName;
		
		// mock code: loading exterior from the file needs 20 ms
		try {
			Thread.sleep(20);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} 
	}
	
	public String getSource(){
		return source;
	}
}
