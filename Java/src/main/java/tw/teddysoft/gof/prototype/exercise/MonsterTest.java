///*
// * Copyright 2016 TeddySoft Technology. All rights reserved.
// *
// */
//package tw.teddysoft.gof.prototype.exercise;
//
//import org.junit.jupiter.api.Test;
//
//import java.util.Date;
//
//public class MonsterTest {
//
//	@Test
//	public void testCreateMonstersAfterApplyingPrototype() {
//
//		int max = 50;
//		Date start = new Date();
//		Monster monster = new Monster(100, 100, "./monster.xml");
//
//		for(int i = 0; i < max; i++){
//			Monster clone = monster.clone();
//		}
//
//		Date end = new Date();
//
//		System.out.println("Create " + max + " Monster needs:" +
//				(end.getTime() - start.getTime()) + " ms");
//	}
//
//
//	@Test
//	public void testMasterBeforePrototype() {
//
//		Monster monster = new Monster(100, 100, "/master.xml");
//
//		System.out.println("Original Master...");
//		monster.display();
//
//		System.out.println("Master was hit.");
//		monster.hit();
//		Monster clone1 = new Monster(monster.getHP(), monster.getPower(), monster.getExterior().getSource());
//		Monster clone2 = new Monster(monster.getHP(), monster.getPower(), monster.getExterior().getSource());
//
//		System.out.println("Clone two master, clone1 and clone 2.");
//
//		System.out.println("Original Master...");
//		monster.display();
//		System.out.println("Clone 1...");
//		clone1.display();
//		System.out.println("Clone 2...");
//		clone2.display();
//
//
//		System.out.println("Master was hit twice.");
//		monster.hit();
//		monster.hit();
//
//		System.out.println("Clone one master, clone3.");
//		Monster clone3 = new Monster(monster.getHP(), monster.getPower(), monster.getExterior().getSource());
//
//		System.out.println("Original Master...");
//		monster.display();
//		System.out.println("Clone 1...");
//		clone1.display();
//		System.out.println("Clone 2...");
//		clone2.display();
//		System.out.println("Clone 3...");
//		clone3.display();
//
//
//	}
//
//
//	@Test
//	public void testMasterClone() {
//
//		Monster master = new Monster(100, 100, "/master.xml");
//		System.out.println("Original Master...");
//		master.display();
//
//		System.out.println("Master was hit.");
//		master.hit();
//		Monster clone1 = master.clone();
//		Monster clone2 = master.clone();
//
//		System.out.println("Clone two master, clone1 and clone 2.");
//
//		System.out.println("Original Master...");
//		master.display();
//		System.out.println("Clone 1...");
//		clone1.display();
//		System.out.println("Clone 2...");
//		clone2.display();
//
//
//		System.out.println("Master was hit twice.");
//		master.hit();
//		master.hit();
//
//		System.out.println("Clone one master, clone3.");
//		Monster clone3 = master.clone();
//
//
//		System.out.println("Original Master...");
//		master.display();
//		System.out.println("Clone 1...");
//		clone1.display();
//		System.out.println("Clone 2...");
//		clone2.display();
//		System.out.println("Clone 3...");
//		clone3.display();
//
//	}
//
//
//}
